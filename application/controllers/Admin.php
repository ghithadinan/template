<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin
 *
 * @author Gets
 */
class Admin extends Admin_Controller {

    private $class_name;
    private $route = 'admin#pages/';
    private $path = 'assets/files/image/avatar/';

    public function __construct() {
        parent::__construct();
        $this->class_name = strtolower(get_class($this));

        $this->load->model(array(
            $this->class_name . '_model',
            'authority_model',
            'authority_detail_model',
            'menu_model',
            'group_menu_model'
        ));
    }

    public function index() {
        $this->data['action'] = site_url($this->class_name . '/search');
        $this->data['route'] = $this->route;

        // begin get authority menu
        $cond_menu = array();

        $authority_view_menu = $this->get_authority_menu('view', 1);
        $id_view_menu = implode("', '", $authority_view_menu);

        $cond_menu["id IN ('$id_view_menu')"] = NULL;
        $cond_menu['status'] = 1; # active
        $this->data['menu'] = $this->menu_model->get_many_by_order($cond_menu, array('orders' => 'ASC'));

        $cond_group_menu = array();

        $authority_view_group_menu = $this->get_authority_menu('view', 2);
        $authority_view_group_menu_child = $this->get_authority_menu('view', 3);

        $authority_group_menu = array_merge($authority_view_group_menu, $authority_view_group_menu_child);
        $id_view_group_menu = implode("', '", $authority_group_menu);

        $cond_group_menu["id IN ('$id_view_group_menu')"] = NULL;
        $cond_group_menu['status'] = 1; # active
        $this->data['group_menu'] = $this->group_menu_model->get_many_by_order($cond_group_menu, array('orders' => 'ASC'));

        $username = $this->session->userdata('username');
        $admin = $this->admin_model->get_by(array('username' => $username));
        $authority = $this->authority_model->get($admin->authority_id);
        // end get authority menu
        // begin get user data
        $this->data['admin']['name'] = $admin->name;
        $this->data['admin']['authority'] = $authority->name;

        if (!empty($admin->avatar)) {
            $this->data['admin']['avatar'] = base_url($this->path . $admin->avatar);
        } else {
            $this->data['admin']['avatar'] = base_url('assets/files/image/default-medium.png');
        }

        $this->data['admin']['last_login'] = $admin->last_login;
        // end get user data

        $this->load->view('pages/template', $this->data);
    }

    public function search() {
        $search = $this->input->post('q');
        if (isset($search)) {
            redirect(site_url('admin#pages/' . $search));
        }
    }

    private function get_authority_menu($authority, $type) {
        $authority_id = $this->session->userdata('authority_id');
        $authority_menu = $this->authority_detail_model->get_many_by(array('authority_id' => $authority_id, $authority => 't', 'type' => $type));

        $id_menu = array(0);
        if (!empty($authority_menu)) {

            foreach ($authority_menu as $row) {
                $id_menu[] = $row->id_menu;
            }
        }

        return $id_menu;
    }

}
