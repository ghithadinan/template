<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Menu
 *
 * @author gets daniel
 */
class Menu extends Admin_Controller {

    private $class_name;
    private $module;
    private $title = 'Menu';
    private $orders = array('orders' => 'ASC');
    private $limit = 10;

    public function __construct() {
        parent::__construct();
        parent::authority(get_class($this), 'view');

        $this->class_name = strtolower(get_class($this));
        $this->module = 'pages/' . $this->class_name;

        $this->load->library('pagination');
        $this->load->model(array(
            $this->class_name . '_model',
            'group_menu_model'
        ));

        $this->data['module'] = $this->module;
    }

    public function index($offset = '') {
        $this->data['title'] = $this->title;
        # begin pagination
        $this->data['menu'] = $this->menu_model->get_pagination($this->limit, $offset, $this->orders);
        $count = $this->menu_model->get_count();
        $config = my_static::config_page($this->module . '/index', $count, $this->limit, 4);
        $this->pagination->initialize($config);
        $this->data['page'] = $this->pagination->create_links();
        # end pagination

        $this->data['group_menu'] = $this->group_menu_model->get_all_order($this->orders);
        # btn group
        $this->data['btn_group'] = parent::authority(get_class($this), 'add', array(
                    form_button(array(
                        'class' => 'btn btn-default btn-sm',
                        'id' => 'add-modal',
                        'source' => site_url($this->module . '/add'),
                        'data-toggle' => 'tooltip',
                        'title' => 'Add',
                        'content' => '<i class="glyphicon glyphicon-plus"></i> Add'
                    ))
        ));

        # authority
        $this->data['auth_add'] = parent::authority(get_class($this), 'add', TRUE);
        $this->data['auth_edit'] = parent::authority(get_class($this), 'edit', TRUE);
        $this->data['auth_delete'] = parent::authority(get_class($this), 'edit', TRUE);

        $this->load->view($this->module . '/index', $this->data);
    }

    public function add($id = '') {
        parent::authority(get_class($this), 'add');

        $this->data['title'] = '<i class="fa fa-plus"></i> Add ' . $this->title;
        $this->data['action'] = site_url($this->module . '/save/' . encrypt::encode($id));

        if (!empty($id)) {
            $this->data['title'] = '<i class="fa fa-edit"></i> Edit ' . $this->title;
            $this->data['data'] = $this->menu_model->get($id);
        }

        $this->data['next_order'] = $this->menu_model->get_next_order();
        # options
        $this->data['options_menu'] = my_static::status_menu();

        $this->load->view($this->module . '/form_menu', $this->data);
    }

    public function edit($id) {
        parent::authority(get_class($this), 'edit');

        $data = $this->menu_model->get(encrypt::decode($id));

        if (!empty($id) && !empty($data)) {
            $this->add(encrypt::decode($id));
        } else {
            redirect('pages/error404');
        }
    }

    public function save($get_id = '') {
        $id = encrypt::decode($get_id);

        $this->form_validation->set_rules('orders', 'Order', 'numeric|required|callback_check_orders_menu[' . $id . ']');
        $this->form_validation->set_rules('name', 'Name', 'required|callback_check_name_menu[' . $id . ']');
        $this->form_validation->set_rules('status', 'Status', 'required');

        if ($this->form_validation->run() == TRUE) {

            $post = $this->input->post();

            if (empty($post['icon'])) {
                $icon = 'fa-circle-o';
            } else {
                $icon = trim($post['icon']);
            }

            $data = array(
                'orders' => trim($post['orders']),
                'name' => trim($post['name']),
                'icon' => $icon,
                'desc' => trim($post['desc']),
                'status' => trim($post['status']),
            );

            if (empty($id)) { //create
                $this->menu_model->save($data);
                $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Add data success');
            } else { //update
                $this->menu_model->update($data, $id);
                $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Update data success');
            }
        } else {
            $message = array('status' => FALSE, 'message' => $this->form_validation->error_array());
        }

        echo json_encode($message);
    }

    public function check_orders_menu($field, $id) {
        $new_orders = trim($field);

        $check_orders = $this->menu_model->get_by(array('orders' => $new_orders));

        if (!empty($check_orders)) {
            $orders = '';
            if (!empty($id)) {
                $menu = $this->menu_model->get($id);
                $orders = $menu->orders;
            }
            if ($new_orders != $orders) {
                $this->form_validation->set_message('check_orders_menu', "Order $new_orders has been used.");
                return FALSE;
            }
        }
    }

    public function check_name_menu($field, $id) {
        $new_name = trim($field);

        $check_name = $this->menu_model->get_by(array('name' => $new_name));
        $check_name_group = $this->group_menu_model->get_by(array('name' => $new_name));

        if (!empty($check_name) || !empty($check_name_group)) {
            $name = '';
            if (!empty($id)) {
                $menu = $this->menu_model->get($id);
                $name = $menu->name;
            }
            if ($new_name != $name) {
                $this->form_validation->set_message('check_name_menu', "Name $new_name has been used.");
                return FALSE;
            }
        }
    }

    public function delete() {
        parent::authority(get_class($this), 'delete');

        $code = encrypt::decode($this->input->post('code'));

        if ($this->menu_model->get($code)) {
            $this->menu_model->delete($code);
            $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Delete data success', 'success' => 'reload()');
        } else {
            $message = array('info' => 'error', 'status' => FALSE, 'message' => 'Delete data not found');
        }

        echo json_encode($message);
    }

    public function add_group_menu($get_id_menu = '', $id = '') {
        parent::authority(get_class($this), 'add');

        $this->data['title'] = '<i class="fa fa-plus"></i> Add Group ' . $this->title;

        if (!empty($id)) { // edit
            $this->data['title'] = '<i class="fa fa-edit"></i> Edit Group ' . $this->title;
            $this->data['data'] = $this->group_menu_model->get($id);
            $id_menu = encrypt::encode($get_id_menu);
        } else { // add
            $this->data['next_order'] = $this->group_menu_model->get_next_order(encrypt::decode($get_id_menu));
            $id_menu = $get_id_menu;
        }

        $this->data['action'] = site_url($this->module . '/save_group_menu/' . $id_menu . '/' . encrypt::encode($id));

        # options
        $this->data['options_status_menu'] = my_static::status_menu();
        $this->data['options_type_menu'] = my_static::type_menu();

        $this->load->view($this->module . '/form_group_menu', $this->data);
    }

    public function edit_group_menu($get_id) {
        parent::authority(get_class($this), 'edit');

        $id = encrypt::decode($get_id);

        $group_menu = $this->group_menu_model->get($id);

        if (!empty($id) && !empty($group_menu)) {
            $menu = $this->menu_model->get($group_menu->id_menu);
            $this->add_group_menu($menu->id, $id);
        } else {
            redirect('pages/error404');
        }
    }

    public function add_group_menu_child($get_id_menu_parent = '', $id = '') {
        parent::authority(get_class($this), 'add');

        $this->data['title'] = '<i class="fa fa-plus"></i> Add Group ' . $this->title . ' Child';

        if (!empty($id)) { // edit
            $this->data['title'] = '<i class="fa fa-edit"></i> Edit Group ' . $this->title . ' Child';
            $this->data['data'] = $this->group_menu_model->get($id);
            $id_menu_parent = encrypt::encode($get_id_menu_parent);
        } else { // add
            $id_menu_parent = $get_id_menu_parent;
            $this->data['next_order'] = $this->group_menu_model->get_next_order_child(encrypt::decode($id_menu_parent));
        }

        $this->data['action'] = site_url($this->module . '/save_group_menu_child/' . $id_menu_parent . '/' . encrypt::encode($id));

        # options
        $this->data['options_status_menu'] = my_static::status_menu();

        $this->load->view($this->module . '/form_group_menu_child', $this->data);
    }

    public function edit_group_menu_child($get_id) {
        parent::authority(get_class($this), 'edit');

        $id = encrypt::decode($get_id);

        $group_menu = $this->group_menu_model->get($id);

        if (!empty($id) && !empty($group_menu)) {
            $this->add_group_menu_child($group_menu->id_parent, $id);
        } else {
            redirect('pages/error404');
        }
    }

    public function save_group_menu($get_id_menu, $get_id = '') {
        $id_menu = encrypt::decode($get_id_menu);

        $id = encrypt::decode($get_id);

        $param = $id_menu . '||' . $id;

        $this->form_validation->set_rules('orders', 'Order', 'numeric|required|callback_check_orders_group_menu[' . $param . ']');
        $this->form_validation->set_rules('name', 'Name', 'required|callback_check_name_group_menu[' . $param . ']');

        $post = $this->input->post();

        if ($post['type'] == 1) { # menu
            $this->form_validation->set_rules('url', 'Url', 'trim|required|callback_check_url_group_menu[' . $id . ']');
            $url = trim($post['url']);
        } else {
            $url = NULL;
        }

        $this->form_validation->set_rules('status', 'Status', 'trim|required');

        if ($this->form_validation->run()) {

            if (empty($post['icon'])) {
                $icon = 'fa-circle-o';
            } else {
                $icon = trim($post['icon']);
            }

            $data = array(
                'orders' => trim($post['orders']),
                'id_menu' => $id_menu,
                'name' => trim($post['name']),
                'type' => trim($post['type']),
                'url' => $url,
                'icon' => $icon,
                'desc' => trim($post['desc']),
                'status' => trim($post['status'])
            );

            if (empty($id)) { //create
                $this->group_menu_model->save($data);
                $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Add data success');
            } else { //update
                $this->group_menu_model->update($data, $id);
                $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Update data success');
            }
        } else {
            $message = array('status' => FALSE, 'message' => $this->form_validation->error_array());
        }

        echo json_encode($message);
    }

    public function save_group_menu_child($get_id_parent, $get_id = '') {
        $id_parent = encrypt::decode($get_id_parent);

        $id = encrypt::decode($get_id);

        $param = $id_parent . '||' . $id;

        $this->form_validation->set_rules('orders', 'Order', 'numeric|required|callback_check_orders_group_menu_child[' . $param . ']');
        $this->form_validation->set_rules('name', 'Name', 'required|callback_check_name_group_menu_child[' . $param . ']');
        $this->form_validation->set_rules('url', 'Url', 'required|callback_check_url_group_menu[' . $id . ']');
        $this->form_validation->set_rules('status', 'Status', 'required');

        if ($this->form_validation->run()) {

            $group_menu_parent = $this->group_menu_model->get($id_parent);
            $id_menu = $group_menu_parent->id_menu;

            $post = $this->input->post();

            if (empty($post['icon'])) {
                $icon = 'fa-circle-o';
            } else {
                $icon = trim($post['icon']);
            }

            $data = array(
                'orders' => trim($post['orders']),
                'id_menu' => $id_menu,
                'id_parent' => $id_parent,
                'name' => trim($post['name']), # menu
                'url' => trim($post['url']),
                'icon' => $icon,
                'desc' => trim($post['desc']),
                'status' => trim($post['status'])
            );

            if (empty($id)) { //create
                $this->group_menu_model->save($data);
                $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Add data success');
            } else { //update
                $this->group_menu_model->update($data, $id);
                $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Update data success');
            }
        } else {
            $message = array('status' => FALSE, 'message' => $this->form_validation->error_array());
        }

        echo json_encode($message);
    }

    public function check_orders_group_menu($field, $param) {
        $new_orders = trim($field);
        $params = explode("||", $param);

        $check_orders = $this->group_menu_model->get_by(array('id_menu' => $params[0], 'orders' => $new_orders));

        if (!empty($check_orders)) {
            $orders = '';
            if (!empty($params[1])) {
                $menu = $this->group_menu_model->get($params[1]);
                $orders = $menu->orders;
            }
            if ($new_orders != $orders) {
                $this->form_validation->set_message('check_orders_group_menu', "Order $new_orders has been used.");
                return FALSE;
            }
        }
    }

    public function check_name_group_menu($field, $param) {
        $new_name = trim($field);
        $params = explode("||", $param);

        $check_name = $this->menu_model->get_by(array('name' => $new_name));
        $check_name_group = $this->group_menu_model->get_by(array('name' => $new_name));

        $name_group = '';
        if (!empty($check_name_group)) {
            $name_group = $check_name_group->name;
        }

        if (!empty($check_name) || !empty($check_name_group)) {
            $name = '';
            if (!empty($params[1])) {
                $menu = $this->group_menu_model->get($params[1]);
                $name = $menu->name;
            }

            $group_menu = $this->menu_model->get($params[0]);

            if ($new_name != $name && strtolower($new_name) != strtolower($group_menu->name) || $name != $name_group) {
                $this->form_validation->set_message('check_name_group_menu', "Name $new_name has been used.");
                return FALSE;
            }
        }
    }

    public function check_url_group_menu($field, $id) {
        $new_url = trim($field);

        $check_url = $this->group_menu_model->get_by(array('url' => $new_url));

        if (!empty($check_url)) {
            $url = '';
            if (!empty($id)) {
                $menu = $this->group_menu_model->get($id);
                $url = $menu->url;
            }
            if ($new_url != $url) {
                $this->form_validation->set_message('check_url_group_menu', "Url $new_url has been used.");
                return FALSE;
            }
        }
    }

    public function check_orders_group_menu_child($field, $param) {
        $new_orders = trim($field);
        $params = explode("||", $param);

        $check_orders = $this->group_menu_model->get_by(array('id_parent' => $params[0], 'orders' => $new_orders));

        if (!empty($check_orders)) {
            $orders = '';
            if (!empty($params[1])) {
                $menu = $this->group_menu_model->get($params[1]);
                $orders = $menu->orders;
            }
            if ($new_orders != $orders) {
                $this->form_validation->set_message('check_orders_group_menu_child', "Order $new_orders has been used.");
                return FALSE;
            }
        }
    }

    public function check_name_group_menu_child($field, $param) {
        $new_name = trim($field);
        $params = explode("||", $param);

        $check_name = $this->menu_model->get_by(array('name' => $new_name));
        $check_name_group = $this->group_menu_model->get_by(array('name' => $new_name));

        $name_group = '';
        if (!empty($check_name_group)) {
            $name_group = $check_name_group->name;
        }

        if (!empty($check_name) || !empty($check_name_group)) {

            $name = '';
            if (!empty($params[1])) {
                $menu = $this->group_menu_model->get($params[1]);
                $name = $menu->name;
            }

            $parent = $this->group_menu_model->get($params[0]);
            $group_menu = $this->menu_model->get($parent->id_menu);

            if ($new_name != $name && strtolower($new_name) != strtolower($group_menu->name) || $name != $name_group) {
                $this->form_validation->set_message('check_name_group_menu_child', "Name $new_name has been used.");
                return FALSE;
            }
        }
    }

    public function delete_group_menu() {
        parent::authority(get_class($this), 'delete');

        $code = encrypt::decode($this->input->post('code'));

        if ($this->group_menu_model->get($code)) {
            $this->group_menu_model->delete($code);
            $this->group_menu_model->delete_by(array('id_parent' => $code));
            $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Delete data success', 'success' => 'reload()');
        } else {
            $message = array('info' => 'error', 'status' => FALSE, 'message' => 'Delete data not found');
        }

        echo json_encode($message);
    }

}
