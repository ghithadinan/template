<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dashboard
 *
 * @author gets daniel
 */
class Dashboard extends Admin_Controller {

    private $class_name;
    private $module;
    private $title = 'Dashboard';

    public function __construct() {
        parent::__construct();
        parent::authority(get_class($this), 'view');

        $this->class_name = strtolower(get_class($this));
        $this->module = 'pages/' . $this->class_name;
    }

    public function index() {
        $this->data['title'] = $this->title;
        $this->data['class_name'] = $this->class_name;
        $this->load->view($this->module . '/index', $this->data);
    }

}
