<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Authority
 *
 * @author gets daniel
 */
class Authority extends Admin_Controller {

    private $class_name;
    private $module;
    private $title = 'Authority';

    public function __construct() {
        parent::__construct();
        parent::authority(get_class($this), 'view');

        $this->class_name = strtolower(get_class($this));
        $this->module = 'pages/' . $this->class_name;

        $this->load->model(array(
            $this->class_name . '_model',
            $this->class_name . '_detail_model',
            'menu_model',
            'group_menu_model'
        ));
    }

    public function index() {
        $this->data['title'] = $this->title;

        # set heading table
        $tmpl = array(
            'table_open' => '<table class="table table-bordered table-striped" id="table" source="' . site_url($this->module . '/get_list') . '">',
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('No', 'Authority Name', my_static::set_align('Action'));
        $this->data['table_head'] = $this->table->generate();
        # filter
        $this->data['options_authority'] = $this->authority_model->options('Select a Authority Name', '', 'id', 'name');
        # btn group
        $this->data['btn_group'] = parent::authority(get_class($this), 'add', array(
                    form_button(array(
                        'class' => 'btn btn-default btn-sm',
                        'onclick' => 'redirect(this)',
                        'source' => site_url($this->module . '/add'),
                        'data-toggle' => 'tooltip',
                        'title' => 'Add',
                        'content' => '<i class="glyphicon glyphicon-plus"></i> Add'
                    ))
        ));


        $this->load->view($this->module . '/index', $this->data);
    }

    public function get_list() {
        $datatable = $this->authority_model->datatable();
        $data = array();
        $start = $this->input->post('start');

        foreach ($datatable['list'] as $value) {
            $start++;
            $id = encrypt::encode($value->id);

            $row = array();
            $row[] = $start;
            $row[] = $value->name;

            //add html for action
            $button = '';
            $button .= '<div class="button-group">';
            # edit
            $button .= parent::authority(get_class($this), 'edit', form_button(array(
                        'class' => 'btn btn-primary btn-xs',
                        'onclick' => 'redirect(this)',
                        'source' => site_url($this->module . '/edit/' . $id),
                        'data-toggle' => 'tooltip',
                        'title' => 'Edit',
                        'content' => '<i class="glyphicon glyphicon-pencil"></i>'
            )));
            # delete
            $button .= parent::authority(get_class($this), 'delete', form_button(array(
                        'class' => 'btn btn-danger btn-xs',
                        'onclick' => 'delete_data(this)',
                        'code' => $id,
                        'action' => site_url($this->module . '/delete'),
                        'data-toggle' => 'tooltip',
                        'title' => 'Delete',
                        'content' => '<i class="glyphicon glyphicon-trash"></i>'
            )));
            $button .= '</div>';

            $row[] = my_static::set_align($button);

            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $datatable['recordsTotal'],
            "recordsFiltered" => $datatable['recordsFiltered'],
            "data" => $data
        );

        echo json_encode($output);
    }

    public function add($id = '') {
        parent::authority(get_class($this), 'add');

        $this->data['title'] = '<i class="fa fa-plus"></i> Add ' . $this->title;
        $this->data['action'] = site_url($this->module . '/save/' . encrypt::encode($id));
        $this->data['id'] = encrypt::encode($id);

        $this->data['authority_view_menu'] = $this->get_authority_menu($id, 'view', 1);

        $this->data['authority_view_group_menu'] = $this->get_authority_menu($id, 'view', 2);
        $this->data['authority_add_group_menu'] = $this->get_authority_menu($id, 'add', 2);
        $this->data['authority_edit_group_menu'] = $this->get_authority_menu($id, 'edit', 2);
        $this->data['authority_delete_group_menu'] = $this->get_authority_menu($id, 'delete', 2);

        $this->data['authority_view_group_menu_child'] = $this->get_authority_menu($id, 'view', 3);
        $this->data['authority_add_group_menu_child'] = $this->get_authority_menu($id, 'add', 3);
        $this->data['authority_edit_group_menu_child'] = $this->get_authority_menu($id, 'edit', 3);
        $this->data['authority_delete_group_menu_child'] = $this->get_authority_menu($id, 'delete', 3);

        if (!empty($id)) {
            $this->data['title'] = '<i class="fa fa-edit"></i> Edit ' . $this->title;
            $this->data['data'] = $this->authority_model->get($id);
        }

        $this->data['menu'] = $this->menu_model->get_all_order(array('orders' => 'ASC'));
        $this->data['group_menu'] = $this->group_menu_model->get_all_order(array('orders' => 'ASC'));

        $this->load->view($this->module . '/form', $this->data);
    }

    private function get_authority_menu($id, $authority, $type) {
        $authority_menu = $this->authority_detail_model->get_many_by(array('authority_id' => $id, $authority => 't', 'type' => $type));

        $id_menu = array();
        if (!empty($authority_menu)) {

            foreach ($authority_menu as $row) {
                $id_menu[] = $row->id_menu;
            }
        }

        return $id_menu;
    }

    public function edit($get_id) {
        parent::authority(get_class($this), 'edit');

        $id = encrypt::decode($get_id);

        $data = $this->authority_model->get($id);

        if (!empty($id) && !empty($data)) {
            $this->add($id);
        } else {
            redirect('pages/error404');
        }
    }

    public function save($get_id) {
        $id = encrypt::decode($get_id);

        $this->form_validation->set_rules('name', 'Authority Name', 'required|callback_check_name[' . $id . ']');

        if ($this->form_validation->run()) {

            $post = $this->input->post();

            $data = array(
                'name' => trim($post['name']),
                'desc' => !empty($post['desc']) ? $post['desc'] : NULL
            );

            if (empty($id)) { //create
                $authority_id = $this->authority_model->save($data);

                $this->create_authority_detail($authority_id, $post);

                $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Add data success');
            } else { //update
                $this->authority_model->update($data, $id);

                $this->create_authority_detail($id, $post);

                $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Update data success');
            }
        } else {
            $message = array('status' => FALSE, 'message' => validation_errors());
        }

        echo json_encode($message);
    }

    private function create_authority_detail($authority_id, $post) {
        $this->authority_detail_model->delete_by(array('authority_id' => $authority_id));

        $menu = $this->menu_model->get_all();
        $group_menu = $this->group_menu_model->get_all();

        if (!empty($menu)) {
            foreach ($menu as $data_menu) {

                $create_menu = array(
                    'authority_id' => $authority_id,
                    'id_menu' => $data_menu->id,
                    'view' => 'f',
                    'type' => 1
                );

                $this->authority_detail_model->save($create_menu);
            }

            if (!empty($group_menu)) {
                foreach ($group_menu as $data_group_menu) {

                    $type = 3; #child
                    if (empty($data_group_menu->id_parent)) {
                        $type = 2; #parent
                    }

                    $create_group_menu = array(
                        'authority_id' => $authority_id,
                        'id_menu' => $data_group_menu->id,
                        'view' => 'f',
                        'add' => 'f',
                        'edit' => 'f',
                        'delete' => 'f',
                        'type' => $type
                    );

                    if ($data_group_menu->type == 2) { # parent
                        unset($create_group_menu['add'], $create_group_menu['edit'], $create_group_menu['delete']);
                    }

                    $this->authority_detail_model->save($create_group_menu);
                }
            }

            if (!empty($post['view_menu'])) {
                $this->update_authority_detail($post['view_menu'], 'view', $authority_id, 1);
            }

            if (!empty($post['view_group_menu'])) {
                $this->update_authority_detail($post['view_group_menu'], 'view', $authority_id, 2);
            }
            if (!empty($post['add_group_menu'])) {
                $this->update_authority_detail($post['add_group_menu'], 'add', $authority_id, 2);
            }
            if (!empty($post['edit_group_menu'])) {
                $this->update_authority_detail($post['edit_group_menu'], 'edit', $authority_id, 2);
            }
            if (!empty($post['delete_group_menu'])) {
                $this->update_authority_detail($post['delete_group_menu'], 'delete', $authority_id, 2);
            }

            if (!empty($post['view_group_menu_child'])) {
                $this->update_authority_detail($post['view_group_menu_child'], 'view', $authority_id, 3);
            }
            if (!empty($post['add_group_menu_child'])) {
                $this->update_authority_detail($post['add_group_menu_child'], 'add', $authority_id, 3);
            }
            if (!empty($post['edit_group_menu_child'])) {
                $this->update_authority_detail($post['edit_group_menu_child'], 'edit', $authority_id, 3);
            }
            if (!empty($post['delete_group_menu_child'])) {
                $this->update_authority_detail($post['delete_group_menu_child'], 'delete', $authority_id, 3);
            }
        }
    }

    private function update_authority_detail($post, $authority, $authority_id, $type) {
        foreach ($post as $post_data) {

            if (!empty($post_data)) {
                $id_menu = encrypt::decode($post_data);

                $this->authority_detail_model->update_by(array('authority_id' => $authority_id, 'id_menu' => $id_menu, 'type' => $type), array($authority => 't'));
            }
        }
    }

    public function check_name($field, $id) {
        $new_name = trim($field);

        $check_name = $this->authority_model->get_by(array('name' => $new_name));

        if (!empty($check_name)) {
            $name = '';
            if (!empty($id)) {
                $athority = $this->authority_model->get($id);
                $name = $athority->name;
            }
            if ($new_name != $name) {
                $this->form_validation->set_message('check_name', "Name $new_name has been used.");
                return FALSE;
            }
        }
    }

    public function delete() {
        parent::authority(get_class($this), 'delete');

        $code = encrypt::decode($this->input->post('code'));

        if ($this->authority_model->get($code)) {
            $this->authority_model->delete($code);
            $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Delete data success', 'success' => 'reload_datatable("#table")');
        } else {
            $message = array('info' => 'error', 'status' => FALSE, 'message' => 'Delete data not found');
        }

        echo json_encode($message);
    }

}
