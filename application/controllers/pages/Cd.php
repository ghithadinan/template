<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cd
 *
 * @author gets daniel
 */
class Cd extends Admin_Controller {

    private $class_name;
    private $module;
    private $title = 'CD';
    private $path = 'assets/files/image/cd/';

    public function __construct() {
        parent::__construct();
        parent::authority(get_class($this), 'view');

        $this->class_name = strtolower(get_class($this));
        $this->module = 'pages/' . $this->class_name;

        $this->load->model(array(
            $this->class_name . '_model',
            'artist_model',
            'label_model'
        ));
    }

    public function index() {
        $this->data['title'] = $this->title;

        # set heading table
        $tmpl = array(
            'table_open' => '<table class="table table-bordered table-striped" id="table" source="' . site_url($this->module . '/get_list') . '">',
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('No', 'Artist Name', 'Album Tittle', 'Label Name', 'Release', 'Price', 'Stock', my_static::set_align('Action'));
        $this->data['table'] = $this->table->generate();
        # filter
        $this->data['options_artist'] = $this->artist_model->options('Select a Artist Name', '', 'id_artist', 'nama_artist');
        # btn group
        $this->data['btn_group'] = parent::authority(get_class($this), 'add', array(
                    form_button(array(
                        'class' => 'btn btn-default btn-sm',
                        'onclick' => 'redirect(this)',
                        'source' => site_url($this->module . '/add'),
                        'data-toggle' => 'tooltip',
                        'title' => 'Add',
                        'content' => '<i class="glyphicon glyphicon-plus"></i> Add'
                    ))
        ));

        $this->load->view($this->module . '/index', $this->data);
    }

    public function get_list() {
        $datatable = $this->cd_model->datatable();
        $data = array();
        $start = $this->input->post('start');

        foreach ($datatable['list'] as $value) {
            $start++;
            $id = encrypt::encode($value->kode_cd);

            $row = array();
            $row[] = $start;
            $row[] = $value->nama_artist;
            $row[] = $value->judul_album;
            $row[] = $value->nama_label;
            $row[] = my_static::date($value->release, 1);
            $row[] = my_static::rupiah($value->harga);
            $row[] = $value->stok;

            //add html for action
            $button = '';
            $button .= '<div class="button-group">';
            # edit
            $button .= parent::authority(get_class($this), 'edit', form_button(array(
                        'class' => 'btn btn-primary btn-xs',
                        'onclick' => 'redirect(this)',
                        'source' => site_url($this->module . '/edit/' . $id),
                        'data-toggle' => 'tooltip',
                        'title' => 'Edit',
                        'content' => '<i class="glyphicon glyphicon-pencil"></i>'
            )));
            # detail
            $button .= parent::authority(get_class($this), 'edit', form_button(array(
                        'class' => 'btn btn-info btn-xs details-control',
                        'start' => $start,
                        'source' => site_url($this->module . '/detail/' . $id),
                        'data-toggle' => 'tooltip',
                        'title' => 'Detail',
                        'content' => '<i class="glyphicon glyphicon-plus"></i>'
            )));
            # delete
            $button .= parent::authority(get_class($this), 'delete', form_button(array(
                        'class' => 'btn btn-danger btn-xs',
                        'onclick' => 'delete_data(this)',
                        'code' => $id,
                        'action' => site_url($this->module . '/delete'),
                        'data-toggle' => 'tooltip',
                        'title' => 'Delete',
                        'content' => '<i class="glyphicon glyphicon-trash"></i>'
            )));
            $button .= '</div>';

            $row[] = my_static::set_align($button);

            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $datatable['recordsTotal'],
            "recordsFiltered" => $datatable['recordsFiltered'],
            "data" => $data
        );

        echo json_encode($output);
    }

    public function add($id = '') {
        parent::authority(get_class($this), 'add');

        $this->data['title'] = '<i class="fa fa-plus"></i> Add ' . $this->title;
        $this->data['action'] = site_url($this->module . '/save/' . encrypt::encode($id));
        $this->data['id'] = encrypt::encode($id);
        $this->data['path'] = $this->path;

        if (!empty($id)) {
            $this->data['title'] = '<i class="fa fa-edit"></i> Edit ' . $this->title;
            $this->data['data'] = $this->cd_model->get($id);
        }

        # options
        $this->data['options_artist'] = $this->artist_model->options('Select a Artist Name', '', 'id_artist', 'nama_artist');
        $this->data['options_label'] = $this->label_model->options('Select a Lable Name', '', 'id_label', 'nama_label');

        $this->load->view($this->module . '/form', $this->data);
    }

    public function edit($get_id) {
        parent::authority(get_class($this), 'edit');

        $id = encrypt::decode($get_id);

        $data = $this->cd_model->get($id);

        if (!empty($id) && !empty($data)) {
            $this->add($id);
        } else {
            redirect('pages/error404');
        }
    }

    public function save($get_id) {
        $id = encrypt::decode($get_id);
        $post = $this->input->post();
        $param = $id . '||' . $post['id_label'] . '||' . $post['id_artist'];

        $this->form_validation->set_rules('id_artist', 'Artist Name', 'required');
        $this->form_validation->set_rules('release', 'Release', 'required');
        $this->form_validation->set_rules('judul_album', 'Album Tittle', 'required|callback_check_name[' . $param . ']');
        $this->form_validation->set_rules('id_label', 'Label Name', 'required');
        $this->form_validation->set_rules('harga', 'Price', 'required');
        $this->form_validation->set_rules('stok', 'Stock', 'required|numeric');
        $this->form_validation->set_rules('list_lagu', 'Song List', 'required');
        $this->form_validation->set_rules('keterangan_cd', 'Description', 'required');

        if (empty($_FILES['gambar_cd']['name']) && empty($id)) {
            $this->form_validation->set_rules('gambar_cd', 'Image', 'trim|required');
        }

        if ($this->form_validation->run()) {

            $gambar_cd = '';
            if (!empty($_FILES['gambar_cd']['name'])) {
                $gambar_cd = $this->upload('gambar_cd');
            }

            $data = array(
                'id_artist' => trim($post['id_artist']),
                'release' => trim($post['release']),
                'judul_album' => trim($post['judul_album']),
                'id_label' => trim($post['id_label']),
                'harga' => my_static::switch_number(trim($post['harga'])),
                'stok' => trim($post['stok']),
                'list_lagu' => trim($post['list_lagu']),
                'keterangan_cd' => trim($post['keterangan_cd']),
                'gambar_cd' => $gambar_cd
            );

            if ($gambar_cd == '') {
                unset($data['gambar_cd']);
            }

            if (empty($id)) { //create
                $this->cd_model->save($data);
                $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Add data success');
            } else { //update
                if (!empty($gambar_cd)) {
                    $this->unlink($id);
                }
                $this->cd_model->update($data, $id);
                $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Update data success');
            }
        } else {
            $message = array('status' => FALSE, 'message' => validation_errors());
        }

        echo json_encode($message);
    }

    public function check_name($field, $param) {
        $new_name = trim($field);
        $params = explode('||', $param);

        $check_name = $this->cd_model->get_by(array('judul_album' => $new_name, 'id_label' => $params[1], 'id_artist' => $params[2]));

        if (!empty($check_name)) {
            $name = '';
            $id_label = '';
            if (!empty($params[0])) {
                $cd = $this->cd_model->get($params[0]);
                $name = $cd->judul_album;
                $id_label = $cd->id_label;
                $id_artist = $cd->id_artist;
            }
            if ($new_name != $name || $id_label != $params[1] || $id_artist != $params[2]) {
                $label = $this->label_model->get($params[1]);
                $artist = $this->artist_model->get($params[2]);

                $this->form_validation->set_message('check_name', "Album Tittle $new_name with artist $artist->nama_artist label $label->nama_label has been used.");
                return FALSE;
            }
        }
    }

    private function upload($file_name) {
        $file = $this->do_upload($file_name);

        if (!empty($file['error'])) {
            $message = array('status' => FALSE, 'message' => $file['error']);
            echo json_encode($message);
            exit();
        } else {
            return $file['name'];
        }
    }

    private function do_upload($file_name) {
        $config['upload_path'] = FCPATH . $this->path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 100;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if (!$this->upload->do_upload($file_name)) {
            $file = array('error' => $this->upload->display_errors());
        } else {
            $file = array('name' => $this->upload->file_name);
        }

        return $file;
    }

    private function unlink($id) {
        $cd = $this->cd_model->get($id);
        $gambar_cd = $cd->gambar_cd;

        if (!empty($gambar_cd)) {
            if (file_exists($this->path . $gambar_cd)) {
                unlink($this->path . $gambar_cd);
            }
        }
    }

    public function detail($get_id) {
        $id = encrypt::decode($get_id);
        $data = $this->cd_model->get_data_cd($id);

        if (!empty($get_id) && !empty($data)) {
            $this->data['title'] = '<i class="fa fa-search-plus"></i> Detail ' . $this->title;
            $this->data['data'] = $data;
            $this->data['path'] = $this->path;

            $this->load->view($this->module . '/detail', $this->data);
        } else {
            redirect('pages/error404');
        }
    }

    public function delete() {
        parent::authority(get_class($this), 'delete');

        $post_code = $this->input->post('code');
        $code = encrypt::decode($post_code);

        if ($this->cd_model->get($code)) {
            $this->unlink($code);
            $this->cd_model->delete($code);
            $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Delete data success', 'success' => 'reload_datatable("#table")');
        } else {
            $message = array('info' => 'error', 'status' => FALSE, 'message' => 'Delete data not found');
        }

        echo json_encode($message);
    }

}
