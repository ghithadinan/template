<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Error_404
 *
 * @author gets daniel
 */
class Error_404 extends Admin_Controller {

    private $class_name;
    private $module;
    private $title = '404 Error';

    public function __construct() {
        parent::__construct();
        $this->class_name = strtolower(get_class($this));
        $this->module = 'pages/' . $this->class_name;
    }

    public function index() {
        $this->data['title'] = $this->title;
        $this->data['action'] = site_url($this->module . '/search');

        $this->load->view($this->module, $this->data);
    }

    public function search() {
        $search = $this->input->post('search');
        if (isset($search)) {
            redirect(site_url('admin#pages/' . $search));
        }
    }

}
