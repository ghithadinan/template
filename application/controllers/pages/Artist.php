<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Artist
 *
 * @author gets daniel
 */
class Artist extends Admin_Controller {

    private $class_name;
    private $module;
    private $title = 'Artist';

    public function __construct() {
        parent::__construct();
        parent::authority(get_class($this), 'view');

        $this->class_name = strtolower(get_class($this));
        $this->module = 'pages/' . $this->class_name;

        $this->load->model(array(
            $this->class_name . '_model',
            'genre_model'
        ));
    }

    public function index() {
        $this->data['title'] = $this->title;

        # set heading table
        $tmpl = array(
            'table_open' => '<table class="table table-bordered table-striped" id="table" source="' . site_url($this->module . '/get_list') . '">',
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('No', 'Artist Name', 'Genre Name', my_static::set_align('Action'));
        $this->data['table'] = $this->table->generate();
        # filter
        $this->data['options_artist'] = $this->artist_model->options('Select a Artist Name', '', 'id_artist', 'nama_artist');
        $this->data['options_genre'] = $this->genre_model->options('Select a Genre Name', '', 'id_genre', 'nama_genre');
        # btn group
        $this->data['btn_group'] = parent::authority(get_class($this), 'add', array(
                    form_button(array(
                        'class' => 'btn btn-default btn-sm',
                        'id' => 'add-modal',
                        'source' => site_url($this->module . '/add'),
                        'data-toggle' => 'tooltip',
                        'title' => 'Add',
                        'content' => '<i class="glyphicon glyphicon-plus"></i> Add'
                    ))
        ));

        $this->load->view($this->module . '/index', $this->data);
    }

    public function get_list() {
        $datatable = $this->artist_model->datatable();
        $data = array();
        $start = $this->input->post('start');

        foreach ($datatable['list'] as $value) {
            $start++;
            $id = encrypt::encode($value->id_artist);

            $row = array();
            $row[] = $start;
            $row[] = $value->nama_artist;
            $row[] = $value->nama_genre;

            //add html for action
            $button = '';
            $button .= '<div class="button-group">';
            # edit
            $button .= parent::authority(get_class($this), 'edit', form_button(array(
                        'class' => 'btn btn-primary btn-xs',
                        'onclick' => 'edit_modal(this)',
                        'source' => site_url($this->module . '/edit/' . $id),
                        'data-toggle' => 'tooltip',
                        'title' => 'Edit',
                        'content' => '<i class="glyphicon glyphicon-pencil"></i>'
            )));
            # delete
            $button .= parent::authority(get_class($this), 'delete', form_button(array(
                        'class' => 'btn btn-danger btn-xs',
                        'onclick' => 'delete_data(this)',
                        'code' => $id,
                        'action' => site_url($this->module . '/delete'),
                        'data-toggle' => 'tooltip',
                        'title' => 'Delete',
                        'content' => '<i class="glyphicon glyphicon-trash"></i>'
            )));
            $button .= '</div>';

            $row[] = my_static::set_align($button);

            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $datatable['recordsTotal'],
            "recordsFiltered" => $datatable['recordsFiltered'],
            "data" => $data
        );

        echo json_encode($output);
    }

    public function add($id = '') {
        parent::authority(get_class($this), 'add');

        $this->data['title'] = '<i class="fa fa-plus"></i> Add ' . $this->title;
        $this->data['action'] = site_url($this->module . '/save/' . encrypt::encode($id));

        if (!empty($id)) {
            $this->data['title'] = '<i class="fa fa-edit"></i> Edit ' . $this->title;
            $this->data['data'] = $this->artist_model->get($id);
        }
        # options
        $this->data['options_genre'] = $this->genre_model->options('Select a Genre Name', '', 'id_genre', 'nama_genre');

        $this->load->view($this->module . '/form', $this->data);
    }

    public function edit($get_id) {
        parent::authority(get_class($this), 'edit');

        $id = encrypt::decode($get_id);
        $data = $this->artist_model->get($id);

        if (!empty($id) && !empty($data)) {
            $this->add($id);
        } else {
            redirect('pages/error404');
        }
    }

    public function save($get_id) {
        $id = encrypt::decode($get_id);
        $id_genre = $this->input->post('id_genre');

        $param = $id . '||' . $id_genre;

        $this->form_validation->set_rules('nama_artist', 'Artist Name', 'required|callback_check_name[' . $param . ']');
        $this->form_validation->set_rules('id_genre', 'Genre Name', 'required');

        if ($this->form_validation->run()) {

            $post = $this->input->post();

            $data = array(
                'nama_artist' => trim($post['nama_artist']),
                'id_genre' => trim($post['id_genre'])
            );

            if (empty($id)) { //create
                $this->artist_model->save($data);
                $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Add data success');
            } else { //update
                $this->artist_model->update($data, $id);
                $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Update data success');
            }
        } else {
            $message = array('status' => FALSE, 'message' => $this->form_validation->error_array());
        }

        echo json_encode($message);
    }

    public function check_name($field, $param) {
        $new_name = trim($field);
        $params = explode('||', $param);

        $check_name = $this->artist_model->get_by(array('nama_artist' => $new_name, 'id_genre' => $params[1]));

        if (!empty($check_name)) {
            $name = '';
            $id_genre = '';
            if (!empty($params[0])) {
                $artist = $this->artist_model->get($params[0]);
                $name = $artist->nama_artist;
                $id_genre = $artist->id_genre;
            }
            if ($new_name != $name || $id_genre != $params[1]) {
                $genre = $this->genre_model->get($params[1]);

                $this->form_validation->set_message('check_name', "Name $new_name with genre $genre->nama_genre has been used.");
                return FALSE;
            }
        }
    }

    public function delete() {
        parent::authority(get_class($this), 'edit');

        $code = encrypt::decode($this->input->post('code'));

        if ($this->artist_model->get($code)) {
            $this->artist_model->delete($code);
            $message = array('info' => 'success', 'status' => TRUE, 'message' => 'Delete data success', 'success' => 'reload_datatable("#table")');
        } else {
            $message = array('info' => 'error', 'status' => FALSE, 'message' => 'Delete data not found');
        }

        echo json_encode($message);
    }

}
