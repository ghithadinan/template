<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author gets daniel
 */
class Login extends CI_Controller {

    private $class_name;
    private $module;
    private $title = 'Log in';

    public function __construct() {
        parent::__construct();
        $this->class_name = strtolower(get_class($this));
        $this->module = 'pages/' . $this->class_name;

        $this->load->model('admin_model');

        $this->data['title'] = $this->title;
        $this->data['action'] = site_url($this->class_name . '/process');
    }

    public function index() {

        $this->check_login();
        $this->load->view($this->module . '/index', $this->data);
    }

    public function process() {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $username = $post['username'];
            $password = $post['password'];

            $admin = $this->admin_model->get_by(array('username' => $username, 'password' => md5($password)));

            if (count($admin) == 1) {

                $login = array(
                    'login' => TRUE,
                    'username' => $admin->username,
                    'authority_id' => $admin->authority_id
                );

                $this->session->set_userdata($login);

                $this->admin_model->update(array('last_login' => date('Y-m-d H:i:s')), $admin->id);

                redirect('admin');
            } else {
                $this->session->set_flashdata('message', 'Username and/or Password Invalid');
            }
        } else {
            $this->session->set_flashdata('message', validation_errors());
        }

        $this->load->view($this->module . '/index', $this->data);
    }

    private function check_login() {
        if ($this->session->userdata('login') == TRUE) {
            redirect('admin');
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect($this->class_name);
    }

}
