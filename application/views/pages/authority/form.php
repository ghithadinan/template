<section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
    </ol>
</section>
<?php echo form_open_multipart($action, array('class' => 'form-horizontal', 'id' => 'form-authority')); ?>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Authority Name</label>
                        <div class="col-lg-6">
                            <?php echo form_input('name', !empty($data->name) ? $data->name : '', 'class="form-control" placeholder="Name" autofocus'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Description</label>
                        <div class="col-lg-9">
                            <?php echo form_textarea('description', !empty($data->description) ? $data->description : '', 'placeholder="Description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-lg-2 control-label"></label>
                        <div class="col-lg-9">
                            <div class="box-tools pull-right" style="padding-bottom: 5px;">
                                <?php if (!empty($menu) && !empty($group_menu)): ?>
                                    <button class="btn btn-sm btn-default expand-all" data-toggle="tooltip" title="Expand All"><i class="fa fa-plus"></i> Expand All</button>
                                    <button class="btn btn-sm btn-default collapse-all" data-toggle="tooltip" title="Collapse All"><i class="fa fa-minus"></i> Collapse All</button>
                                <?php endif; ?>
                            </div>
                            <table class="table table-bordered tree">
                                <tr>
                                    <th rowspan="2">Order</th>
                                    <th rowspan="2">Name</th>
                                    <th rowspan="2">Description</th>
                                    <th rowspan="2">Status</th>
                                    <th colspan="4" class="text-center">Access</th>
                                </tr>
                                <tr>
                                    <th class="text-center"><i class="fa fa-eye" data-toggle="tooltip" title="View"></i></th>
                                    <th class="text-center"><i class="fa fa-plus" data-toggle="tooltip" title="Add"></i></th>
                                    <th class="text-center"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></th>
                                    <th class="text-center"><i class="fa fa-trash-o" data-toggle="tooltip" title="Delete"></i></th>
                                </tr>
                                <?php
                                if (!empty($menu)) {
                                    $no = 1;
                                    foreach ($menu as $row):
                                        $id_menu = encrypt::encode($row->id);
                                        ?>
                                        <tr class="treegrid-<?php echo $no; ?>">
                                            <td> <?php echo $row->orders; ?></td>
                                            <td><i class="fa <?php echo $row->icon; ?>"></i> <?php echo $row->name; ?></td>
                                            <td><?php echo!empty($row->desc) ? $row->desc : ''; ?></td>
                                            <td><?php echo my_static::status_menu($row->status); ?></td>
                                            <td class="text-center">
                                                <label>
                                                    <input type="checkbox" name="view_menu[]" value="<?php echo $id_menu; ?>" <?php
                                                    if (in_array(encrypt::decode($id_menu), $authority_view_menu)) : echo 'checked';
                                                    endif;
                                                    ?>>
                                                </label>
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <?php
                                        if (!empty($group_menu)):
                                            $parent = $no;
                                            foreach ($group_menu as $row2):
                                                if ($row2->id_menu == $row->id && empty($row2->id_parent)) :
                                                    $no++;
                                                    $id_group_menu = encrypt::encode($row2->id);

                                                    $action_url = 'edit_group_menu';
                                                    if (!empty($row2->id_parent)):
                                                        $action_url = 'edit_group_menu_child';
                                                    endif;

                                                    $parent_group_menu_child = 0;
                                                    if ($row2->type == 2):
                                                        $parent_group_menu_child = $no;
                                                    endif;
                                                    ?>
                                                    <tr class="treegrid-<?php echo $no; ?> treegrid-parent-<?php echo $parent; ?>">
                                                        <td> <?php echo $row2->orders; ?></td>
                                                        <td><i class="fa <?php echo $row2->icon; ?>"></i> <?php echo $row2->name; ?></td>
                                                        <td><?php echo!empty($row2->desc) ? $row2->desc : ''; ?></td>
                                                        <td><?php echo my_static::status_menu($row2->status); ?></td>
                                                        <td class="text-center">
                                                            <label>
                                                                <input type="checkbox" name="view_group_menu[]" value="<?php echo $id_group_menu; ?>" <?php
                                                                if (in_array(encrypt::decode($id_group_menu), $authority_view_group_menu)) : echo 'checked';
                                                                endif;
                                                                ?>>
                                                            </label>
                                                        </td>

                                                        <?php if ($parent_group_menu_child == 0) { ?>
                                                            <td class="text-center">
                                                                <label>
                                                                    &nbsp;<input type="checkbox" name="add_group_menu[]" value="<?php echo $id_group_menu; ?>" <?php
                                                                    if (in_array(encrypt::decode($id_group_menu), $authority_add_group_menu)) : echo 'checked';
                                                                    endif;
                                                                    ?>>
                                                                </label>
                                                            </td>
                                                            <td class="text-center">
                                                                <label>
                                                                    &nbsp;<input type="checkbox" name="edit_group_menu[]" value="<?php echo $id_group_menu; ?>" <?php
                                                                    if (in_array(encrypt::decode($id_group_menu), $authority_edit_group_menu)) : echo 'checked';
                                                                    endif;
                                                                    ?>>
                                                                </label>
                                                            </td>
                                                            <td class="text-center">
                                                                <label>
                                                                    &nbsp;<input type="checkbox" name="delete_group_menu[]" value="<?php echo $id_group_menu; ?>" <?php
                                                                    if (in_array(encrypt::decode($id_group_menu), $authority_delete_group_menu)) : echo 'checked';
                                                                    endif;
                                                                    ?>>
                                                                </label>
                                                            </td>
                                                        <?php } else { ?> 
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        <?php } ?>

                                                    </tr>
                                                    
                                                    <?php
                                                    foreach ($group_menu as $row3):
                                                        if ($row3->id_parent == $row2->id) :
                                                            $no++;
                                                            $id_group_menu_child = encrypt::encode($row3->id);
                                                            ?>
                                                            <tr class="treegrid-<?php echo $no; ?> treegrid-parent-<?php echo $parent_group_menu_child; ?>">
                                                                <td> <?php echo $row3->orders; ?></td>
                                                                <td><i class="fa <?php echo $row3->icon; ?>"></i> <?php echo $row3->name; ?></td>
                                                                <td><?php echo!empty($row3->desc) ? $row3->desc : ''; ?></td>
                                                                <td><?php echo my_static::status_menu($row3->status); ?></td>
                                                                <td class="text-center">
                                                                    <label>
                                                                        <input type="checkbox" name="view_group_menu_child[]" value="<?php echo $id_group_menu_child; ?>" <?php
                                                                        if (in_array(encrypt::decode($id_group_menu_child), $authority_view_group_menu_child)) : echo 'checked';
                                                                        endif;
                                                                        ?>>
                                                                    </label>
                                                                </td>
                                                                <td class="text-center">
                                                                    <label>
                                                                        &nbsp;<input type="checkbox" name="add_group_menu_child[]" value="<?php echo $id_group_menu_child; ?>" <?php
                                                                        if (in_array(encrypt::decode($id_group_menu_child), $authority_add_group_menu_child)) : echo 'checked';
                                                                        endif;
                                                                        ?>>
                                                                    </label>
                                                                <td class="text-center">
                                                                    <label>
                                                                        &nbsp;<input type="checkbox" name="edit_group_menu_child[]" value="<?php echo $id_group_menu_child; ?>" <?php
                                                                        if (in_array(encrypt::decode($id_group_menu_child), $authority_edit_group_menu_child)) : echo 'checked';
                                                                        endif;
                                                                        ?>>
                                                                    </label>
                                                                </td>
                                                                <td class="text-center">
                                                                    <label>
                                                                        &nbsp;<input type="checkbox" name="delete_group_menu_child[]" value="<?php echo $id_group_menu_child; ?>" <?php
                                                                        if (in_array(encrypt::decode($id_group_menu_child), $authority_delete_group_menu_child)) : echo 'checked';
                                                                        endif;
                                                                        ?>>
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        endif;
                                                    endforeach;
                                                endif;
                                            endforeach;
                                        endif;
                                        $no++;
                                    endforeach;
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="5">
                                            No data available in table
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="button" class="btn btn-default btn-sm" onclick="reload()" data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left"></i> Back</button>
            <button type="submit" class="btn btn-primary btn-sm pull-right" data-toggle="tooltip" title="Save"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>
</section>
<?php echo form_close(); ?>
<script type="text/javascript">
    //form validation
    $('#form-authority').MyFormValidation({
        type: 'alert',
        success: function () {
            reload();
        }
    });

    $(function () {
        $('.tree').treegrid({
            initialState: 'collapse'
        });

        $('.expand-all').show();
        $('.collapse-all').hide();

        $('.expand-all').click(function (e) {
            e.preventDefault();

            $('.tree').treegrid();
            $(this).hide();
            $('.collapse-all').show();
        });

        $('.collapse-all').click(function (e) {
            e.preventDefault();

            $('.tree').treegrid({
                initialState: 'collapse'
            });
            $(this).hide();
            $('.expand-all').show();
        });
    });
</script>