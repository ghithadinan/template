<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo $title; ?></h3>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th width="30%">Artist Name</th>
                <td><?php echo $data->nama_artist; ?></td>
            </tr>
            <tr>
                <th>Album Name</th>
                <td><?php echo $data->judul_album; ?></td>
            </tr>
            <tr>
                <th>Label Name</th>
                <td><?php echo $data->nama_label; ?></td>
            </tr>
            <tr>
                <th>Release</th>
                <td><?php echo my_static::date($data->release, 1); ?></td>
            </tr>
            <tr>
                <th>Price</th>
                <td><?php echo my_static::rupiah($data->harga); ?></td>
            </tr>
            <tr>
                <th>Stock</th>
                <td><?php echo $data->stok; ?></td>
            </tr>
            <tr>
                <th>Song List</th>
                <td><?php echo $data->list_lagu; ?></td>
            </tr>
            <tr>
                <th>Description</th>
                <td><?php echo $data->keterangan_cd; ?></td>
            </tr>
            <tr>
                <th>Image</th>
                <td><img src="<?php echo $path . $data->gambar_cd; ?>" height="189px" width="300px"></td>
            </tr>
        </table>
    </div>
</div>