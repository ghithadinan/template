<section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
    </ol>
</section>
<?php echo form_open_multipart($action, array('class' => 'form-horizontal', 'id' => 'form-cd')); ?>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Artist Name</label>
                        <div class="col-lg-6">
                            <?php echo form_dropdown('id_artist', $options_artist, !empty($data->id_artist) ? $data->id_artist : '', 'class="form-control" autofocus'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Relase</label>
                        <div class="col-lg-6">
                            <?php echo form_input('release', !empty($data->release) ? $data->release : '', 'id="date" class="form-control" placeview="Release" readonly'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Album Tittle</label>
                        <div class="col-lg-6">
                            <?php echo form_input('judul_album', !empty($data->judul_album) ? $data->judul_album : '', 'class="form-control" placeview="Album Tittle"'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Lable Name</label>
                        <div class="col-lg-6">
                            <?php echo form_dropdown('id_label', $options_label, !empty($data->id_label) ? $data->id_label : '', 'class="form-control"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Price</label>
                        <div class="col-lg-6">
                            <?php echo form_input('harga', !empty($data->harga) ? $data->harga : '', 'class="form-control money" placeview="Price"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Stock</label>
                        <div class="col-lg-3">
                            <?php echo form_input('stok', !empty($data->stok) ? $data->stok : '', 'class="form-control numeric" placeview="Stock"'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Song List</label>
                        <div class="col-lg-9">
                            <?php echo form_textarea('list_lagu', !empty($data->list_lagu) ? $data->list_lagu : '', 'class="textarea" placeview="Song List" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Description</label>
                        <div class="col-lg-9">
                            <?php echo form_textarea('keterangan_cd', !empty($data->keterangan_cd) ? $data->keterangan_cd : '', 'class="textarea" placeview="Description" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Image</label>
                        <div class="col-lg-3">
                            <?php if (!empty($data->gambar_cd)) { ?>
                                <img src="<?php echo base_url($path . $data->gambar_cd); ?>" id="exist-image" height="189px" width="300px">
                            <?php } else { ?>
                                <img src="<?php echo base_url('assets/files/image/empty.png'); ?>" id="empty-image" height="189px" width="300px">
                            <?php } ?>
                            <div id="image-view"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"></label>
                        <div class="col-lg-3">
                            <?php echo form_upload('gambar_cd', '', 'class="form-control" id="file-upload"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="button" class="btn btn-default btn-sm" onclick="reload()" data-toggle="tooltip" title="Back"><i class="fa fa-arrow-left"></i> Back</button>
            <button type="submit" class="btn btn-primary btn-sm pull-right" data-toggle="tooltip" title="Save"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>
</section>
<?php echo form_close(); ?>
<script type="text/javascript">
    $(function () {
        $('#date').datepicker({
            format: 'yyyy-mm-dd'
        });
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
        $('.numeric').Numeric();
        $('.money').number(true, 2);
    });
    //form validation
    $('#form-cd').MyFormValidation({
        type: 'alert',
        success: function () {
            reload();
        }
    });

    $('#file-upload').on('change', function () {
        //Get count of selected files
        var value = $(this).val();
        var countFiles = $(this)[0].files.length;
        var imgPath = $(this)[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        var image_view = $('#image-view');
        var empty_image = $('#empty-image');
        var exist_image = $('#exist-image');
        var message = null;

        image_view.empty();

        if (value !== '') {
            if (extn === 'gif' || extn === 'png' || extn === 'jpg' || extn === 'jpeg') {
                if (typeof (FileReader) !== undefined) {
                    //loop for each file selected for uploaded.
                    for (var i = 0; i < countFiles; i++) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('<img />', {
                                src: e.target.result,
                                class: 'thumb-image',
                                height: '198px',
                                width: '300px'
                            }).appendTo(image_view);
                        };

                        if (exist_image.val() === undefined) {
                            empty_image.hide();
                        } else {
                            exist_image.hide();
                        }

                        image_view.show();
                        reader.readAsDataURL($(this)[0].files[i]);
                    }
                } else {
                    message = 'This browser does not support FileReader.';
                }
            } else {
                message = 'Please select only images.';
            }

            if (message !== null) {
                bootbox.dialog({
                    title: '<i class="fa fa-bullhorn"></i> Info',
                    message: '<div class="alert alert-danger"><h4>Warning!</h4>' + message + '</div>',
                    buttons: {
                        main: {
                            label: 'OK',
                            className: 'btn btn-primary'
                        }
                    }
                });

                $(this).val('');

                if (exist_image.val() === undefined) {
                    empty_image.show();
                } else {
                    exist_image.show();
                }
            }
        } else {
            if (exist_image.val() === undefined) {
                empty_image.show();
            } else {
                exist_image.show();
            }
        }
    });
</script>