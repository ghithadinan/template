<section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-gears"></i> <?php echo $title; ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-header with-borders">
            <h3 class="box-title"><?php echo my_static::render_btn_group($btn_group); ?></h3>
            <div class="box-tools pull-right">
                <?php if (!empty($menu) && !empty($group_menu)): ?>
                    <button class="btn btn-sm btn-default expand-all" data-toggle="tooltip" title="Expand All"><i class="fa fa-plus"></i> Expand All</button>
                    <button class="btn btn-sm btn-default collapse-all" data-toggle="tooltip" title="Collapse All"><i class="fa fa-minus"></i> Collapse All</button>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered tree">
                        <tr>
                            <th>Order</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                        <?php
                        if (!empty($menu)) {
                            $no = 1;
                            foreach ($menu as $row):
                                $id_menu = encrypt::encode($row->id);
                                ?>
                                <tr class="treegrid-<?php echo $no; ?>">
                                    <td> <?php echo $row->orders; ?></td>
                                    <td><i class="fa <?php echo $row->icon; ?>"></i> <?php echo $row->name; ?></td>
                                    <td><?php echo!empty($row->desc) ? $row->desc : ''; ?></td>
                                    <td><?php echo my_static::status_menu($row->status); ?></td>
                                    <td class="text-center"> 
                                        <div class="button-group">
                                            <?php if ($auth_add == TRUE) : ?>
                                                <button class="btn btn-default btn-xs" id="add-modal-group" source="<?php echo site_url($module . '/add_group_menu/' . $id_menu); ?>" data-toggle="tooltip" title="Add"><i class="glyphicon glyphicon-plus"></i></button>
                                            <?php endif; ?>
                                            <?php if ($auth_edit == TRUE) : ?>
                                                <button class="btn btn-primary btn-xs" onclick="edit_modal(this)" source="<?php echo site_url($module . '/edit/' . $id_menu); ?>" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></button>
                                            <?php endif; ?>
                                            <?php if ($auth_delete == TRUE) : ?>
                                                <button class="btn btn-danger btn-xs" onclick="delete_data(this)" code="<?php echo $id_menu; ?>" action="<?php echo site_url($module . '/delete'); ?>" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></button>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                if (!empty($group_menu)):
                                    $parent = $no;
                                    foreach ($group_menu as $row2):
                                        if ($row2->id_menu == $row->id && empty($row2->id_parent)) :
                                            $no++;
                                            $id_group_menu = encrypt::encode($row2->id);

                                            $action_url = 'edit_group_menu';
                                            if (!empty($row2->id_parent)):
                                                $action_url = 'edit_group_menu_child';
                                            endif;

                                            $parent_group_menu_child = 0;
                                            if ($row2->type == 2):
                                                $parent_group_menu_child = $no;
                                            endif;
                                            ?>
                                            <tr class="treegrid-<?php echo $no; ?> treegrid-parent-<?php echo $parent; ?>">
                                                <td> <?php echo $row2->orders; ?></td>
                                                <td><i class="fa <?php echo $row2->icon; ?>"></i> <?php echo $row2->name; ?></td>
                                                <td><?php echo!empty($row2->desc) ? $row2->desc : ''; ?></td>
                                                <td><?php echo my_static::status_menu($row2->status); ?></td>
                                                <td class="text-center">
                                                    <?php if ($auth_add == TRUE) : ?>
                                                        <?php if ($row2->type == 2): ?>
                                                            <button class="btn btn-default btn-xs" id="add-modal-group" source="<?php echo site_url($module . '/add_group_menu_child/' . $id_group_menu); ?>" data-toggle="tooltip" title="Add"><i class="glyphicon glyphicon-plus"></i></button>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php if ($auth_edit == TRUE) : ?>
                                                        <button class="btn btn-primary btn-xs" onclick="edit_modal(this)" source="<?php echo site_url($module . '/' . $action_url . '/' . $id_group_menu); ?>" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></button>
                                                    <?php endif; ?>
                                                    <?php if ($auth_delete == TRUE) : ?>
                                                        <button class="btn btn-danger btn-xs" onclick="delete_data(this)" code="<?php echo $id_group_menu; ?>" action="<?php echo site_url($module . '/delete_group_menu'); ?>" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></button>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <?php
                                            foreach ($group_menu as $row3):
                                                if ($row3->id_parent == $row2->id) :
                                                    $no++;
                                                    $id_group_menu_child = encrypt::encode($row3->id);
                                                    ?>
                                                    <tr class="treegrid-<?php echo $no; ?> treegrid-parent-<?php echo $parent_group_menu_child; ?>">
                                                        <td> <?php echo $row3->orders; ?></td>
                                                        <td><i class="fa <?php echo $row3->icon; ?>"></i> <?php echo $row3->name; ?></td>
                                                        <td><?php echo!empty($row3->desc) ? $row3->desc : ''; ?></td>
                                                        <td><?php echo my_static::status_menu($row3->status); ?></td>
                                                        <td class="text-center">
                                                            <?php if ($auth_edit == TRUE) : ?>
                                                                <button class="btn btn-primary btn-xs" onclick="edit_modal(this)" source="<?php echo site_url($module . '/edit_group_menu_child/' . $id_group_menu_child); ?>" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></button>
                                                            <?php endif; ?>
                                                            <?php if ($auth_delete == TRUE) : ?>
                                                                <button class="btn btn-danger btn-xs" onclick="delete_data(this)" code="<?php echo $id_group_menu_child; ?>" action="<?php echo site_url($module . '/delete_group_menu'); ?>" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></button>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                endif;
                                            endforeach;
                                        endif;
                                    endforeach;
                                endif;
                                $no++;
                            endforeach;
                        } else {
                            ?>
                            <tr>
                                <td colspan="5">
                                    No data available in table
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                    <div class="pull-right">
                        <?php echo my_static::render_page($page); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $('#add-modal, #add-modal-group').click(function () {
            load_modal($(this), '#myModal');
        });

        $('.tree').treegrid();
        $('.expand-all').hide();

        $('.expand-all').click(function () {
            $('.tree').treegrid();
            $(this).hide();
            $('.collapse-all').show();
        });

        $('.collapse-all').click(function () {
            $('.tree').treegrid({
                initialState: 'collapse'
            });
            $(this).hide();
            $('.expand-all').show();
        });

        pagination();
    });
</script>




