<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title" id="myModalLabel"><?php echo $title; ?></h4>
</div>
<?php echo form_open($action, array('class' => 'form-horizontal', 'id' => 'form-group-menu')); ?>
<div class="modal-body">
    <div class="form-group form-group-sm">
        <label class="col-sm-3 control-label">Order</label>
        <div class="col-sm-6">
            <?php echo form_input('orders', !empty($data->orders) ? $data->orders : $next_order, 'class="form-control numeric" placeholder="Order"'); ?>
        </div>
    </div>
    <div class="form-group form-group-sm">
        <label class="col-sm-3 control-label">Name</label>
        <div class="col-sm-6">
            <?php echo form_input('name', !empty($data->name) ? $data->name : '', 'class="form-control" placeholder="Name" autofocus'); ?>
        </div>
    </div>
    <div class="form-group form-group-sm">
        <label class="col-sm-3 control-label">Url</label>
        <div class="col-sm-6">
            <?php echo form_input('url', !empty($data->url) ? $data->url : '', 'class="form-control" placeholder="Url"'); ?>
        </div>
    </div>
    <div class="form-group form-group-sm">
        <label class="col-sm-3 control-label">Icon</label>
        <div class="col-sm-6">
            <?php echo form_input('icon', !empty($data->icon) ? $data->icon : '', 'class="form-control" placeholder="Icon"'); ?>
        </div>
    </div>
    <div class="form-group form-group-sm">
        <label class="col-sm-3 control-label">Description</label>
        <div class="col-sm-6">
            <?php echo form_textarea('desc', !empty($data->desc) ? $data->desc : '', 'class="form-control" placeholder="Description"'); ?>
        </div>
    </div>
    <div class="form-group form-group-sm">
        <label class="col-sm-3 control-label">Status</label>
        <div class="col-sm-6">
            <?php echo form_dropdown('status', $options_status_menu, !empty($data->status) ? $data->status : 1, 'class="form-control"'); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $(function () {
        //form validation
        $('#form-group-menu').MyFormValidation({
            success: function () {
                $('#myModal').modal('hide');
                reload();
            }
        });

        $('.numeric').Numeric();
    });
</script>