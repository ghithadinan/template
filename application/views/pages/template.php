<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <!-- Font Awesome -->
        <link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <!-- Ionicons -->
        <link href="<?php echo base_url('assets/plugins/ionicons/css/ionicons.min.css'); ?>" rel="stylesheet" type="text/css"/>
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/css/skins/skin-blue.min.css'); ?>">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/flat/blue.css'); ?>">
        <!-- Date Picker -->
        <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datepicker/datepicker3.css'); ?>">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="<?php echo base_url('assets/plugins/daterangepicker/daterangepicker-bs3.css'); ?>">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>">
        <!-- Datatables -->
        <link href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet" type="text/css"/>
        <!-- Select2 -->
        <link href="<?php echo base_url('assets/plugins/select2/select2.css'); ?>" rel="stylesheet" type="text/css"/>
        <!-- Jquery Confirm -->
        <link href="<?php echo base_url('assets/plugins/jquery-confirm/jquery-confirm.css'); ?>" rel="stylesheet" type="text/css"/>
        <!-- Toasrt -->
        <link href="<?php echo base_url('assets/plugins/toasrt/toastr.css'); ?>" rel="stylesheet" type="text/css"/>
        <!-- Nprogress -->
        <link href="<?php echo base_url('assets/plugins/nprogress/nprogress.css'); ?>" rel="stylesheet" type="text/css"/>
        <!-- Treegrid -->
        <link href="<?php echo base_url('assets/plugins/treegrid/jquery.treegrid.css'); ?>" rel="stylesheet" type="text/css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/css/AdminLTE.css'); ?>">
        <!-- Custom CSS -->
        <link href="<?php echo base_url('assets/custom/css/custom.css'); ?>" rel="stylesheet" type="text/css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php $this->load->view('pages/header'); ?>
            <?php $this->load->view('pages/sidebar'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- content -->
                <div id="content"></div>
            </div><!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.3.0
                </div>
                <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
            </footer>
        </div><!-- ./wrapper -->

        <!-- Modal -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content"></div>
            </div>
        </div>

        <!-- jQuery 2.1.4 -->
        <script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.1.4.min.js'); ?>"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="<?php echo base_url('assets/plugins/jQueryUI/jquery-ui.min.js'); ?>" type="text/javascript"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <!-- daterangepicker -->
        <script src="<?php echo base_url('assets/plugins/moment/moment.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/daterangepicker/daterangepicker.js'); ?>"></script>
        <!-- datepicker -->
        <script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
        <!-- Slimscroll -->
        <script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url('assets/plugins/fastclick/fastclick.min.js'); ?>"></script>
        <!-- Select2 -->
        <script src="<?php echo base_url('assets/plugins/select2/select2.full.min.js'); ?>" type="text/javascript"></script>
        <!-- Datatables -->
        <script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js'); ?>" type="text/javascript"></script>
        <!-- Jquery Confirm -->
        <script src="<?php echo base_url('assets/plugins/jquery-confirm/jquery-confirm.js'); ?>" type="text/javascript"></script>
        <!-- Toasrt -->
        <script src="<?php echo base_url('assets/plugins/toasrt/toastr.js'); ?>" type="text/javascript"></script>
        <!-- Nprogress -->
        <script src="<?php echo base_url('assets/plugins/nprogress/nprogress.js'); ?>" type="text/javascript"></script>
        <!-- Bootbox -->
        <script src="<?php echo base_url('assets/plugins/bootbox/bootbox.min.js'); ?>" type="text/javascript"></script>
        <!-- Treegrid -->
        <script src="<?php echo base_url('assets/plugins/treegrid/jquery.treegrid.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/treegrid/jquery.treegrid.bootstrap3.js'); ?>" type="text/javascript"></script>
        <!-- Number -->
        <script src="<?php echo base_url('assets/plugins/number/jquery.number.js'); ?>" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url('assets/adminLTE/js/app.js'); ?>"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url('assets/adminLTE/js/demo.js'); ?>"></script>
        <!-- Custom -->
        <script src="<?php echo base_url('assets/custom/js/main.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/custom/js/global.js'); ?>" type="text/javascript"></script>
    </body>
</html>
