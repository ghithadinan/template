<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo $admin['avatar']; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $admin['name']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <?php echo form_open($action, 'class="sidebar-form"'); ?>
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search..." required>
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
        </div>
        <?php echo form_close(); ?>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <?php
            if (!empty($menu)):
                foreach ($menu as $row):
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa <?php echo $row->icon; ?>"></i> <span><?php echo $row->name; ?></span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu"> 
                            <?php
                            if (!empty($group_menu)):
                                foreach ($group_menu as $row2):
                                    if ($row2->id_menu == $row->id && empty($row2->id_parent)):

                                        $href = site_url($route . $row2->url);
                                        $fa_parent = '';
                                        if ($row2->type == 2) {
                                            $fa_parent = '<i class="fa fa-angle-left pull-right"></i>';
                                            $href = '#';
                                        }
                                        ?>
                                        <li>
                                            <a href="<?php echo $href; ?>" title="<?php echo $row2->name; ?>"><i class="fa <?php echo $row2->icon; ?>"></i> <?php echo $row2->name; ?> <?php echo $fa_parent; ?></a>
                                            <?php if (!empty($fa_parent)): ?>
                                                <ul class="treeview-menu">
                                                <?php endif; ?>

                                                <?php
                                                foreach ($group_menu as $row3):
                                                    if ($row3->id_parent == $row2->id):
                                                        ?>
                                                        <li><a href="<?php echo site_url($route . $row3->url); ?>" title="<?php echo $row3->name; ?>"><i class="fa <?php echo $row3->icon; ?>"></i> <?php echo $row3->name; ?></a></li>
                                                            <?php
                                                        endif;
                                                    endforeach;
                                                    ?>

                                                <?php if (!empty($fa_parent)): ?>
                                                </ul>
                                            <?php endif; ?>
                                        </li>
                                        <?php
                                    endif;
                                endforeach;
                            endif;
                            ?>
                        </ul>
                    </li>
                    <?php
                endforeach;
            endif;
            ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>