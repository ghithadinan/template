<section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-gears"></i> <?php echo $title; ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo !empty($btn_group) ? my_static::render_btn_group($btn_group) : ''; ?></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-xs btn-box-tool-table" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Label Name</label>
                        <?php echo form_dropdown('', $options_label, '', 'class="form-control select search-input-text-table" data-column="0" style="width:100%;"'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <?php echo $table_head; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        //datatable
        $('#table').MyDataTable();
        //select input
        $('.select').select2();
        //add modals
        $('#add-modal').click(function () {
            load_modal($(this), '#myModal');
        });
    });
</script>




