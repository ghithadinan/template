<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title" id="myModalLabel"><?php echo $title; ?></h4>
</div>
<?php echo form_open($action, array('class' => 'form-horizontal', 'id' => 'form-artist')); ?>
<div class="modal-body">
    <div class="form-group form-group-sm">
        <label class="col-sm-3 control-label">Artist Name</label>
        <div class="col-sm-6">
            <?php echo form_input('nama_artist', !empty($data->nama_artist) ? $data->nama_artist : '', 'class="form-control" placeholder="Nama Artist" autofocus'); ?>
        </div>
    </div>
    <div class="form-group form-group-sm">
        <label class="col-sm-3 control-label">Genre Name</label>
        <div class="col-sm-6">
            <?php echo form_dropdown('id_genre', $options_genre, !empty($data->id_genre) ? $data->id_genre : '', 'class="form-control"'); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    //form validation
    $('#form-artist').MyFormValidation({
        success: function () {
            $('#myModal').modal('hide');
            reload_datatable('#table');
        }
    });
</script>