<section class="content-header">
    <h1>
        <?php echo $title; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><?php echo $title; ?></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
            <p>
                We could not find the page you were looking for.
                Meanwhile, you may <a href="<?php echo site_url('admin'); ?>">return to dashboard</a> or try using the search form.
            </p>
            <?php echo form_open($action, 'class="search-form"'); ?>
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search" required>
                <div class="input-group-btn">
                    <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>
                </div>
            </div><!-- /.input-group -->
            <?php echo form_close(); ?>
        </div><!-- /.error-content -->
    </div><!-- /.error-page -->
</section><!-- /.content -->