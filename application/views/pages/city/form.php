<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title" id="myModalLabel"><?php echo $title; ?></h4>
</div>
<?php echo form_open($action, array('class' => 'form-horizontal', 'id' => 'form-city')); ?>
<div class="modal-body">
    <div class="form-group form-group-sm">
        <label class="col-sm-3 control-label">City Name</label>
        <div class="col-sm-6">
            <?php echo form_input('nama_kota', !empty($data->nama_kota) ? $data->nama_kota : '', 'class="form-control" placeholder="City Name" autofocus'); ?>
        </div>
    </div>
    <div class="form-group form-group-sm">
        <label class="col-sm-3 control-label">Delivery Pirce</label>
        <div class="col-sm-6">
            <?php echo form_input('ongkos_kirim', !empty($data->ongkos_kirim) ? $data->ongkos_kirim : '', 'class="form-control money" placeholder="Delivery Price"'); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    //form validation
    $('#form-city').MyFormValidation({
        confirm: false,
        success: function () {
            $('#myModal').modal('hide');
            reload_datatable('#table');
        }
    });

    $('.money').number(true, 2);
</script>