<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cd_model
 *
 * @author gets daniel
 */
class Cd_model extends MY_Model {

    private $table = 'cd'; # begin set MY_Model
    private $pk = 'kode_cd';
    private $timestamp = TRUE;
    private $set_order = array('nama_artist' => 'ASC'); # end set MY_Model
    private $column = array('a.id_artist', 'a.release', 'a.judul_album', 'a.id_label', 'a.harga', 'a.stok'); # begin datatable
    private $order = array('a.kode_cd', 'DESC');
    private $artist = 'artist';
    private $label = 'label'; # end datatable

    public function __construct() {
        parent::__construct();
        parent::set_table($this->table, $this->pk, $this->timestamp, $this->set_order);
    }

    //datatable
    private function _query() {
        $query[] = $this->db->select('a.*, b.nama_artist, c.nama_label');
        $query[] = $this->db->from($this->table . ' a');
        $query[] = $this->db->join($this->artist . ' b', 'b.id_artist = a.id_artist', 'left');
        $query[] = $this->db->join($this->label . ' c', 'c.id_label = a.id_label', 'left');

        return $query;
    }

    private function _get_datatables_order($query = array()) { # ordering field datatable
        # search datatable
        $item = $this->column;
        $requestData = $_REQUEST;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $query[] = $this->db->where($item[0], $requestData['columns'][0]['search']['value']);
        }
        if (!empty($requestData['columns'][1]['search']['value'])) {
            $query[] = $this->db->where($item[1], $requestData['columns'][1]['search']['value']);
        }
        if (!empty($requestData['columns'][2]['search']['value'])) {
            $query[] = $this->db->where($item[2], $requestData['columns'][2]['search']['value']);
        }
        if (!empty($requestData['columns'][3]['search']['value'])) {
            $query[] = $this->db->where($item[3], $requestData['columns'][3]['search']['value']);
        }
        if (!empty($requestData['columns'][4]['search']['value'])) {
            $query[] = $this->db->where($item[4], $requestData['columns'][4]['search']['value']);
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {
            $query[] = $this->db->where($item[5], $requestData['columns'][5]['search']['value']);
        }

        # default sort
        if (isset($this->order)) {
            $default = implode(' ', $this->order);
            $query[] = $this->db->order_by($default);
        }

        return $query;
    }

    private function list_datatable($query) {
        $this->_get_datatables_order($query);
        # ordering field datatable
        $length = $this->input->post('length');

        if ($length != -1) {
            $start = $this->input->post('start');
            $this->db->limit($length, $start);
        }

        $list = $this->db->get()->result();

        return $list;
    }

    private function count_datatable($query, $param = '') {
        if ($param == 'filter') {
            $this->_get_datatables_order($query);
        }

        $count = $this->db->get()->num_rows();
        return $count;
    }

    public function datatable() {
        $data = array(
            'list' => $this->list_datatable($this->_query()),
            'recordsTotal' => $this->count_datatable($this->_query()),
            'recordsFiltered' => $this->count_datatable($this->_query(), 'filter')
        );

        return $data;
    }

    //end datatable

    public function get_data_cd($id) {
        $this->_query();
        $this->db->where('a.kode_cd', $id);
        $query = $this->db->get()->row();

        return $query;
    }

}
