<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_model
 *
 * @author gets daniel
 */
class Admin_model extends MY_Model {

    private $table = 'admin'; # begin set MY_Model
    private $pk = 'id';
    private $timestamp = TRUE; # end set MY_Model
    
    public function __construct() {
        parent::__construct();
        parent::set_table($this->table, $this->pk, $this->timestamp);
    }

}
