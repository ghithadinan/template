<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Authority_detail_model
 *
 * @author gets daniel
 */
class Authority_detail_model extends MY_Model {
    
    private $table = 'authority_detail'; # begin set MY_Model
    private $pk = 'id';  # end set MY_Model

    public function __construct() {
        parent::__construct();
        parent::set_table($this->table, $this->pk);
    }
}
