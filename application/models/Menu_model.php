<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Menu_model
 *
 * @author gets daniel
 */
class Menu_model extends MY_Model {

    private $table = 'menu'; # begin set MY_Model
    private $pk = 'id';
    private $timestamp = TRUE; # end set MY_Model

    public function __construct() {
        parent::__construct();
        parent::set_table($this->table, $this->pk, $this->timestamp);
    }

    public function get_next_order() {
        $query = $this->db->query("SELECT MAX(orders) AS last FROM $this->table")->row();
        $next = 1;
        if (!empty($query)) {
            $max = $query->last;
            $next = $max + 1;
        }
        
        return $next;
    }
    
}
