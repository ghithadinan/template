<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Group_menu_model
 *
 * @author gets daniel
 */
class Group_menu_model extends MY_Model {

    private $table = 'group_menu'; # begin set MY_Model
    private $pk = 'id';
    private $timestamp = TRUE; # end set MY_Model

    public function __construct() {
        parent::__construct();
        parent::set_table($this->table, $this->pk, $this->timestamp);
    }

    public function get_next_order($id_menu) {
        $query = $this->db->query("SELECT MAX(orders) AS last FROM $this->table WHERE id_menu = $id_menu")->row();
        $next = 1;
        if (!empty($query)) {
            $max = $query->last;
            $next = $max + 1;
        }
        return $next;
    }
    
    public function get_next_order_child($id_parent) {
        $query = $this->db->query("SELECT MAX(orders) AS last FROM $this->table WHERE id_parent = $id_parent")->row();
        $next = 1;
        if (!empty($query)) {
            $max = $query->last;
            $next = $max + 1;
        }
        return $next;
    }
}
