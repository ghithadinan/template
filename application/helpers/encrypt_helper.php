<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of encrypt_helper
 *
 * @author gets daniel
 */
class encrypt {

    public static $key = 'sdflsdfase34534sdfpweroawewauascvasvasd23423asdkwjerawds';

    public static function encode($param, $url_safe = TRUE) {

        $CI = & get_instance();
        $ret = $CI->encrypt->encode($param, self::$key);

        if ($url_safe) {
            $ret = strtr(
                    $ret, array(
                '+' => '.',
                '=' => '-',
                '/' => '~'
                    )
            );
        }

        return $ret;
    }

    public static function decode($param) {

        $CI = & get_instance();

        $string = strtr(
                $param, array(
            '.' => '+',
            '-' => '=',
            '~' => '/'
                )
        );

        return $CI->encrypt->decode($string, self::$key);
    }

}
