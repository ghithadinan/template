<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of my_static_helper
 *
 * @author gets daniel
 */
class my_static {

    public static $nama_bulan = array('Jan' => 'Januari', 'Feb' => 'Februari', 'Mar' => 'Maret', 'Apr' => 'April', 'May' => 'Mei', 'Jun' => 'Juni', 'Jul' => 'Juli', 'Aug' => 'Agustus', 'Sep' => 'September', 'Oct' => 'Oktober', 'Nov' => 'November', 'Dec' => 'Desember');
    public static $status_menu = array('1' => 'Active', '2' => 'Not Active');
    public static $type_menu = array('1' => 'Menu', '2' => 'Parent');

    public static function merge_array($array1 = array(), $array2 = array()) {
        $array = array();
        foreach ($array1 as $key => $value) {
            $array[$key] = $value;
        }
        foreach ($array2 as $key => $value) {
            $array[$key] = $value;
        }
        return $array;
    }

    public static function date($tanggal, $format = '') {
        if (!empty($tanggal)) {
            switch ($format) {
                case 1:
                    list($a, $b, $c) = explode('-', date('d-M-Y', strtotime($tanggal)));
                    $b = self::$nama_bulan[$b];
                    $date = $a . ' ' . $b . ' ' . $c;
                    break;
                case 2:
                    list($a, $b, $c) = explode('-', $tanggal);
                    $date = $c . '/' . $b . '/' . $a;
                    break;
                default:
                    list($a, $b, $c) = explode('-', $tanggal);
                    $date = $c . '-' . $b . '-' . $a;
                    break;
            }
            return $date;
        } else {
            return '';
        }
    }

    public static function rupiah($number) {
        return is_numeric($number) ? 'Rp.' . number_format($number, 2, ',', '.') : '';
    }

    public static function switch_number($number) {
        $pecah_koma = str_replace(',', '', $number);
        $uang = explode('.', $pecah_koma);
        return !empty($uang[0]) ? $uang[0] : 0;
    }

    public static function set_align($data, $align = 'center') {
        $html = '<div align="' . $align . '">';
        $html .= '' . $data . '';
        $html .= '<div>';

        return $html;
    }

    public static function render_btn_group($list = array()) {
        $bg = '';
        foreach ($list as $value) {
            $bg .= '<div class="btn-group">';
            $bg .= $value;
            $bg .= '</div>';
        }
        return $bg;
    }

    public static function status_menu($kode = '', $default = '--Status Menu--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$status_menu);
            } else {
                return self::$status_menu;
            }
        } else {
            return isset(self::$status_menu[$kode]) ? self::$status_menu[$kode] : '-';
        }
    }
    
    public static function type_menu($kode = '', $default = '--Type Menu--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$type_menu);
            } else {
                return self::$type_menu;
            }
        } else {
            return isset(self::$type_menu[$kode]) ? self::$type_menu[$kode] : '-';
        }
    }

    public static function render_page($data) {
        $page = '';
        if (!empty($data)) {
            $page .= $data;
        } else {
            $page .= '<ul class="pagination pagination-sm">';
            $page .= '<li class="active">';
            $page .= '<a href="javascript:void(0)">1<span class="sr-only"></span></a>';
            $page .= '<li>';
            $page .= '<ul>';
        }

        return $page;
    }

    public static function config_page($url, $count, $limit, $uri_segment) {
        $config['base_url'] = site_url($url);
        $config['total_rows'] = $count;
        $config['per_page'] = $limit;
        $config['uri_segment'] = $uri_segment;
        $config['use_page_numbers'] = TRUE;
        /* This Application Must Be Used With BootStrap 3 *  */
        $config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = "<li class='ci-page'>";
        $config['num_tag_close'] = "</li>";
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='javascript:void(0)'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li class='ci-page'>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li class='ci-page'>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li class='ci-page'>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li class='ci-page'>";
        $config['last_tagl_close'] = "</li>";

        return $config;
    }

}
