<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Model
 *
 * @author gets daniel
 */
class MY_Model extends CI_Model {

    private $table;
    private $pk;
    private $timestamp;

    public function __construct() {
        parent::__construct();
    }

    protected function set_table($table = '', $pk = '', $timestamp = '') {
        $this->table = $table;
        $this->pk = $pk;
        $this->timestamp = $timestamp;

        return $this;
    }

    public function get($id = '') {
        $this->db->where($this->pk, $id);
        $query = $this->_get()->row();

        return $query;
    }

    public function get_all() {
        return $this->_get()->result();
    }

    public function get_all_order($orders = array()) {
        foreach ($orders as $field => $type) {
            $this->db->order_by($field, $type);
        }
        return $this->_get()->result();
    }

    public function get_array() {
        return $this->_get()->result_array();
    }

    public function get_by($param) {
        $query = FALSE;

        if (is_array($param)) {
            $this->db->where($param);
            $query = $this->_get()->row();
        }

        return $query;
    }

    public function get_many_by($param) {
        $query = FALSE;

        if (is_array($param)) {
            $this->db->where($param);
            $query = $this->_get()->result();
        }

        return $query;
    }

    public function get_many_by_order($param, $orders = array()) {
        $query = FALSE;

        if (is_array($param)) {
            $this->db->where($param);
            foreach ($orders as $field => $type) {
                $this->db->order_by($field, $type);
                $query = $this->_get()->result();
            }
        }

        return $query;
    }

    public function get_many_by_array($param) {
        $query = FALSE;

        if (is_array($param)) {
            $this->db->where($param);
            $query = $this->_get->get_array();
        }

        return $query;
    }

    public function get_limit($limit, $param = array()) {
        if (is_array($param)) {
            $this->db->where($param);
        }

        $this->db->limit($limit);
        $query = $this->_get()->result();

        return $query;
    }

    public function get_count($param = array()) {
        if (is_array($param)) {
            $this->db->where($param);
        }

        $query = $this->db->count_all($this->table);

        return $query;
    }

    public function save($data, $id = NULL) {
        if ($id == NULL) { //insert
            if ($this->timestamp == TRUE) {
                $time = date('Y-m-d H:i:s');
                $timestamp = array('created_at' => $time);
                $data = array_merge($data, $timestamp);
            }
            !isset($data[$this->pk]) || $data[$this->pk = NULL];
            $this->db->set($data);
            $this->db->insert($this->table);
            $id = $this->db->insert_id();
        } else { //update
            if ($this->timestamp == TRUE) {
                $time = date('Y-m-d H:i:s');
                $timestamp = array('updated_at' => $time);
                $data = array_merge($data, $timestamp);
            }

            $this->db->set($data);
            $this->db->where($this->pk, $id);
            $this->db->update($this->table);
        }

        return $id;
    }

    public function insert($data = array()) {
        $query = FALSE;

        if ($this->db->insert($this->table, $data)) {
            $query = $this->db->insert_id();
        }

        return $query;
    }

    public function update($data = array(), $id = 0) {
        $query = FALSE;

        if ($this->timestamp == TRUE) {
            $time = date('Y-m-d H:i:s');
            $timestamp = array('updated_at' => $time);
            $data = array_merge($data, $timestamp);
        }

        $this->db->where(array($this->pk => $id));
        if ($this->db->update($this->table, $data)) {
            $query = TRUE;
        }

        return $query;
    }

    public function update_by($where = array(), $data = array()) {
        $query = FALSE;

        $this->db->where($where);
        if ($this->db->update($this->table, $data)) {
            $query = TRUE;
        }

        return $query;
    }

    public function delete($id = 0) {
        $query = FALSE;

        if ($this->db->delete($this->table, array($this->pk => $id))) {
            $query = TRUE;
        }

        return $query;
    }

    public function delete_by($where = array()) {
        $query = FALSE;

        if ($this->db->delete($this->table, $where)) {
            $query = TRUE;
        }

        return $query;
    }

    public function autoid($inisial) {
        $date = date('Ymd');
        $today = $inisial . '' . $date;
        $query = $this->db->query("SELECT MAX($this->pk) AS last FROM $this->table WHERE $this->pk LIKE '$today%'")->row_array();
        $last_id = substr($query['last'], 1, 13);
        $last_no_id = substr($last_id, 8, 4);
        $next_no_id = $last_no_id + 1;
        $next_id = $date . sprintf('%04s', $next_no_id);
        $next_autoid = $inisial . '' . $next_id;

        return $next_autoid;
    }

    public function next_id() {
        $query = $this->db->query("SELECT MAX($this->pk) AS last FROM $this->table")->row_array();
        $last_id = $query['last'];
        $next_id = $last_id + 1;

        return $next_id;
    }

    public function options($default, $key, $options_value, $options_name, $order = 'ASC', $encrypt = '') {
        $this->db->order_by($options_name, $order);
        $data = $this->get_all();

        $options = array();

        if (!empty($default)) {
            $options[$key] = $default;
        }

        foreach ($data as $row) {
            $value = $row->$options_value;
            
            if ($encrypt == TRUE) {
                $value = encrypt::encode($row->$options_value);
            }
            
            $options[$value] = $row->$options_name;
        }
        
        return $options;
    }

    public function get_pagination($limit, $offset, $orders, $param = array()) {
        if (is_null($offset) || empty($offset)) {
            $offset = 0;
        } else {
            $offset = ($offset * $limit) - $limit;
        }

        if (empty($orders) || empty($orders)) {
            $this->db->order_by($this->pk, 'DESC');
        } else {
            foreach ($orders as $orders => $type) {
                $this->db->order_by($orders, $type);
            }
        }

        if (is_array($param)) {
            $this->db->where($param);
        }

        $query = $this->db->get($this->table, $limit, $offset)->result();

        return $query;
    }

    protected function _get() {
        return $this->db->get($this->table);
    }

}
