<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Controller
 *
 * @author gets daniel
 */
class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

}

class Admin_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->check_login();
    }

    private function check_login() {
        if ($this->session->userdata('login') != TRUE) {
            redirect('login');
        }
    }

    protected function authority($class, $authority, $button = NULL) {
        $this->load->model(array(
            'group_menu_model',
            'authority_detail_model'
        ));

        $authority_id = $this->session->userdata('authority_id');

        $group_menu = $this->group_menu_model->get_by(array('name' => strtolower($class)));
        if (!empty($group_menu)) {
            $id_menu = $group_menu->id;

            $type = 3; # child
            if ($group_menu->type == 1) {
                $type = 2; # parent
            }

            $access = $this->authority_detail_model->get_by(array('authority_id' => $authority_id, 'id_menu' => $id_menu, 'type' => $type));

            if ($access->$authority == 'f' && empty($button)) {
                redirect('pages/error404');
            } elseif ($access->$authority == 't' && !empty($button)) {
                return $button;
            }
        }
    }

}
