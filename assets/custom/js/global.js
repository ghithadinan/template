$(function () {
    check_url();
    active_menu();

    $(window).on('hashchange', function () {
        check_url();
        remove_active();
        active_menu();
    });

    $('.treeview').on('mouseenter, mouseleave', function () {
        active_menu();
    });

    login_time();

    setInterval(function () {
        login_time();
    }, 60000);
});

function check_url() {
    var route = 'pages/dashboard';
    var module = location.href.split("#").splice(1).join("#");

    if (module !== '') {
        load_url(module);
    } else {
        window.location.hash = route;
    }
}

function load_url(module) {
    var content = $("#content");
    var url = module;

    content.load(url, function (responseTxt, statusTxt, xhr) {
        NProgress.start();

        if (statusTxt === 'success') {
            NProgress.done();
        }

        if (statusTxt === 'error') {
            $(this).MyAlert({
                info: 'error',
                message: xhr.statusText
            });

        }

        $(this).find('[autofocus]').focus();
    });
}

function active_menu() {
    var base_url = window.location.href;
    var split = base_url.split('/');
    var content = location.href.split("#").splice(1).join("#");
    var module = content.split('/')[1];
    var url = split[0] + '//' + split[2] + '/' + split[3] + '/' + split[4] + '/' + module;

    $('ul.treeview-menu a[href="' + url + '"]').parent().addClass('active');
    $('ul.treeview-menu a[href="' + url + '"]').closest('.treeview').addClass('active');

    var app_name = 'AdminLTE 2';
    var title = $('ul.treeview-menu').children('.active').find('a').attr('title');

    if (title === undefined) {
        title = '404 Error';
    }

    $('title').html(app_name + ' | ' + title);
}

function remove_active() {
    $('.treeview-menu').find('.active').removeClass('active');
    $('.treeview').find('.active').removeClass('active');
}

// begin crud
function edit_modal(index) {
    load_modal(index, '#myModal');
}

function load_modal(index, modal) {
    var content = $(index).attr('source');
    $('.modal-content').load(content, function (responseTxt, statusTxt, xhr) {
        if (statusTxt === 'success') {
            $(modal).modal({show: true});
        }
        if (statusTxt === 'error') {
            $(this).MyAlert({
                info: 'error',
                message: xhr.statusText
            });
        }
    });
}

//delete data
function delete_data(index) {
    var code = $(index).attr('code');
    var action = $(index).attr('action');
    $(index).MyConfirm({
        theme: 'supervan',
        title: '<i class="fa fa-warning"></i> Confirm',
        content: 'Are you sure to delete this data ?',
        autoClose: null,
        confirmButton: '<i class="fa fa-check"></i> Yes',
        cancelButton: '<i class="fa fa-close"></i> No',
        confirm: function () {
            $.ajax({
                url: action,
                type: 'POST',
                dataType: 'json',
                data: 'code=' + code,
                cache: false,
                success: function (data) {
                    if (data.status === true) {
                        eval(data.success);
                    }
                    $(this).MyAlert({
                        info: data.info,
                        message: data.message
                    });
                },
                error: function (data) {
                    $(this).MyAlert({
                        info: 'error',
                        message: data.statusText
                    });
                }
            });
        },
        cancel: function () {
        }
    });
}

function reload_datatable(index) {
    var table = $(index).DataTable();
    table.ajax.reload(null, false);
}

function reload() {
    check_url();
}

function redirect(index) {
    var content = $('#content');
    var url = $(index).attr('source');

    load_url(url);
}

function pagination() {
    $('.ci-page').click(function (e) {
        e.preventDefault();

        var url = $(this).find('a').attr('href');
        load_url(url);
    });
}
// end crud

function login_time() {
    var login = $('#login');
    var login_time = moment(login.attr('value'));
    var last_login = moment(login_time).fromNow();

    login.html(last_login);
}

$(document).on('shown.bs.modal', '.modal', function () {
    $(this).find('[autofocus]').focus();
});