(function ($) {
    $.fn.MyDataTable = function (options) {

        var settings = $.extend({
            defaultFilter: false
        }, options);

        return this.each(function () {
            var dataSource = $(this).attr('source');
            var baseUrl = window.location.origin + window.location.pathname;
            var splitUrl = baseUrl.split('/');
            var imageUrl = '' + window.location.origin + '/' + splitUrl[3] + '/assets/img/ajax-loader.gif';

            var table = $(this).DataTable({
                processing: true,
                language: {
                    processing: '<img src="' + imageUrl + '">'
                },
                serverSide: true,
                ajax: {
                    url: dataSource,
                    type: "POST"
                },
                bSort: false //disable sort
            });

            if (settings.defaultFilter === false) {
                $('#table_filter').hide();
            }

            function search(i, v) {
                table.column(i).search(v).draw();
            }

            var name = $(this).attr('id');

            $('#' + name + ' tbody').on('click', '.details-control', function (e) {
                e.preventDefault();

                var tr = $(this).closest('tr');
                var row = table.row(tr);
                var source = $(this).attr('source');
                var start = $(this).attr('start');

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                    $(this).attr('data-original-title', 'Detail').html('<i class="glyphicon glyphicon-plus"></i>');
                } else {
                    row.child(format(name, start)).show();
                    load_child(name, start, source);
                    tr.addClass('shown');
                    $(this).attr('data-original-title', 'Close').html('<i class="glyphicon glyphicon-minus"></i>');
                }
            });

            function format(name, start) {
                return '<div id="row-details-' + name + '-' + start + '"></div>';
            }

            function load_child(name, start, source) {
                var detail = $('#row-details-' + name + '-' + start);

                detail.html('<img src="' + imageUrl + '" height="20px" width="20px">');
                $.ajax({
                    type: 'GET',
                    url: source,
                    data: null,
                    success: function (data) {
                        detail.html(data);
                    },
                    error: function (data) {
                        $(this).MyAlert({
                            info: 'error',
                            message: data.statusText
                        });
                    }
                });
            }

            $('.search-input-text-' + name).on('change ,keyup, click', function () {
                var i = $(this).attr('data-column');
                var v = $(this).val();
                search(i, v);
            });

            $('.btn-box-tool-' + name).click(function () {
                var fa = $(this).html();
                $('.search-input-text-' + name).each(function () {
                    var i = $(this).attr('data-column');
                    if (fa === '<i class="fa fa-minus"></i>') {
                        search(i, "");
                    } else if (fa === '<i class="fa fa-plus"></i>') {
                        var v = $(this).val();
                        search(i, v);
                    }
                });
            });
        });
    };
}(jQuery));

(function ($) {
    $.fn.MyConfirm = function (options) {
        var settings = $.extend({
            theme: null,
            title: null,
            content: null,
            autoClose: null,
            confirmButton: 'Yes i agree',
            cancelButton: 'NO never !',
            confirm: null,
            cancel: null
        }, options);

        $.confirm({
            theme: settings.theme,
            title: settings.title,
            content: settings.content,
            autoClose: settings.autoClose,
            confirmButton: settings.confirmButton,
            cancelButton: settings.cancelButton,
            confirm: settings.confirm,
            cancel: settings.cancel
        });
    };
}(jQuery));

(function ($) {
    $.fn.Numeric = function () {
        return this.each(function () {
            //called when key is pressed in textbox
            $(this).keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
            $(this).bind('paste', function (e) {
                e.preventDefault();
            });
        });
    };
}(jQuery));

//alert
(function ($) {
    var i = -1;
    var toastCount = 0;
    var $toastlast;

    var getMessage = function () {
        var msgs = ['My name is Inigo Montoya. You killed my father. Prepare to die!',
            '<div><input class="input-small" value="textbox"/>&nbsp;<a href="http://johnpapa.net" target="_blank">This is a hyperlink</a></div><div><button type="button" id="okBtn" class="btn btn-primary">Close me</button><button type="button" id="surpriseBtn" class="btn" style="margin: 0 8px 0 8px">Surprise me</button></div>',
            'Are you the six fingered man?',
            'Inconceivable!',
            'I do not think that means what you think it means.',
            'Have fun storming the castle!'
        ];
        
        i++;
        if (i === msgs.length) {
            i = 0;
        }

        return msgs[i];
    };

    var getMessageWithClearButton = function (msg) {
        msg = msg ? msg : 'Clear itself?';
        msg += '<br /><br /><button type="button" class="btn clear">Yes</button>';
        return msg;
    };

    $.fn.MyAlert = function (options) {

        var settings = $.extend({
            info: null,
            mesage: null
        }, options);

        return this.each(function () {

            var shortCutFunction = settings.info;
            var msg = settings.message;
            var title = 'Info';
            var $showDuration = '300';
            var $hideDuration = '1000';
            var $timeOut = '5000';
            var $extendedTimeOut = '1000';
            var $showEasing = 'swing';
            var $hideEasing = 'linear';
            var $showMethod = 'fadeIn';
            var $hideMethod = 'fadeOut';
            var toastIndex = toastCount++;
            var addClear = false;

            toastr.options = {
                closeButton: true,
                debug: false,
                newestOnTop: false,
                progressBar: false,
                positionClass: 'toast-top-right',
                preventDuplicates: false,
                onclick: null
            };

            if ($('#addBehaviorOnToastClick').prop('checked')) {
                toastr.options.onclick = function () {
                    alert('You can perform some custom action after a toast goes away');
                };
            }

            if ($showDuration.length) {
                toastr.options.showDuration = $showDuration;
            }

            if ($hideDuration.length) {
                toastr.options.hideDuration = $hideDuration;
            }

            if ($timeOut.length) {
                toastr.options.timeOut = addClear ? 0 : $timeOut;
            }

            if ($extendedTimeOut.length) {
                toastr.options.extendedTimeOut = addClear ? 0 : $extendedTimeOut;
            }

            if ($showEasing.length) {
                toastr.options.showEasing = $showEasing;
            }

            if ($hideEasing.length) {
                toastr.options.hideEasing = $hideEasing;
            }

            if ($showMethod.length) {
                toastr.options.showMethod = $showMethod;
            }

            if ($hideMethod.length) {
                toastr.options.hideMethod = $hideMethod;
            }

            if (addClear) {
                msg = getMessageWithClearButton(msg);
                toastr.options.tapToDismiss = false;
            }
            if (!msg) {
                msg = getMessage();
            }

            $('#toastrOptions').text('Command: toastr["'
                    + shortCutFunction
                    + '"]("'
                    + msg
                    + (title ? '", "' + title : '')
                    + '")\n\ntoastr.options = '
                    + JSON.stringify(toastr.options, null, 2)
                    );

            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
            $toastlast = $toast;

            if (typeof $toast === 'undefined') {
                return;
            }

            if ($toast.find('#okBtn').length) {
                $toast.delegate('#okBtn', 'click', function () {
                    alert('you clicked me. i was toast #' + toastIndex + '. goodbye!');
                    $toast.remove();
                });
            }
            if ($toast.find('#surpriseBtn').length) {
                $toast.delegate('#surpriseBtn', 'click', function () {
                    alert('Surprise! you clicked me. i was toast #' + toastIndex + '. You could perform an action here.');
                });
            }
            if ($toast.find('.clear').length) {
                $toast.delegate('.clear', 'click', function () {
                    toastr.clear($toast, {force: true});
                });
            }
        });
    };
}(jQuery));

(function ($) {
    $.fn.MyFormValidation = function (options) {

        var settings = $.extend({
            type: 'onfield',
            success: null,
            confirm: true,
            theme: 'supervan',
            title: '<i class="fa fa-warning"></i> Confirm',
            content: 'Are you sure to save this data ?',
            autoClose: null,
            confirmButton: '<i class="fa fa-check"></i> Yes',
            cancelButton: '<i class="fa fa-close"></i> No'
        }, options);

        return this.each(function () {
            //form validation
            $(this).submit(function (e) {
                e.preventDefault();

                $(this).find(':button').prop('disabled', true);

                resetErrors();

                var data = new FormData($(this)[0]);
                var url = $(this).attr('action');
                var form_id = $(this).attr('id');

                if (settings.confirm === true) {
                    $(this).MyConfirm({
                        theme: settings.theme,
                        title: settings.title,
                        content: settings.content,
                        autoClose: settings.autoClose,
                        confirmButton: settings.confirmButton,
                        cancelButton: settings.cancelButton,
                        confirm: function (data) {
                            $(data).click(function () {
                                post_data();
                            });
                        },
                        cancel: function (data) {
                            $(data).click(function () {
                                $('#' + form_id).find(':button').removeAttr('disabled');
                            });
                        }
                    });

                    $('.jconfirm').click(function () {
                        $('#' + form_id).find(':button').removeAttr('disabled');
                    });

                } else {
                    post_data();
                }

                function post_data() {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: data,
                        dataType: 'json',
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (resp) {
                            if (resp.status === true) {
                                $(this).submit();
                                if (settings.success) {
                                    //return function
                                    settings.success.call($(this));
                                }
                                //successful validation
                                $(this).MyAlert({
                                    info: resp.info,
                                    message: resp.message
                                });

                                return false;
                            } else {
                                if (settings.type === 'onfield') {
                                    $.each(resp.message, function (i, v) {
                                        var msg = '<label class="control-label error" for="' + i + '">' + v + '</label>';
                                        $('input[name="' + i + '"], select[name="' + i + '"], textarea[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                        $('input[name="' + i + '"], select[name="' + i + '"], textarea[name="' + i + '"]').after(msg);
                                    });

                                    var keys = Object.keys(resp.message);
                                    $('input[name="' + keys[0] + '"]').focus();

                                    $('#' + form_id).find(':button').removeAttr('disabled');
                                } else if (settings.type === 'alert') {
                                    bootbox.dialog({
                                        title: '<i class="fa fa-bullhorn"></i> Info',
                                        message: '<div class="alert alert-danger"><h4>Warning!</h4>' + resp.message + '</div>',
                                        buttons: {
                                            main: {
                                                label: '<i class="fa fa-check"></i> OK',
                                                className: 'btn btn-primary btn-sm',
                                                callback: function () {
                                                    var form_field = $('#' + form_id + ' input, #' + form_id + ' select , #' + form_id + ' textarea');

                                                    form_field.each(function () {
                                                        if ($(this).val() === '') {
                                                            $(this).closest('.form-group').addClass('has-error');
                                                        }
                                                    });

                                                    $('#' + form_id).find(':button').removeAttr('disabled');
                                                }

                                            }
                                        }
                                    });

                                    $('.bootbox-close-button').click(function () {
                                        $('#' + form_id).find(':button').removeAttr('disabled');
                                    });
                                }
                            }

                            return false;
                        },
                        error: function (resp) {
                            $(this).MyAlert({
                                info: 'error',
                                message: resp.statusText
                            });
                        }
                    });

                    return false;
                }
            });
        });
    };
}(jQuery));

function resetErrors() {
    $('form input, form select, form textarea').closest('.form-group').removeClass('has-error');
    $('label.error').remove();
}

function auto_close(index, time) {
    //1000 1 second
    var second = time * 1000;
    window.setTimeout(function () {
        $(index).fadeTo(1500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, second);
}