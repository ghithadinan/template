-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 25, 2016 at 09:36 
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cd`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `authority_id` int(11) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `avatar` varchar(200) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `username`, `password`, `authority_id`, `last_login`, `avatar`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Ghitha Dinan', 'admin', '21232f297a57a5a743894a0e4a801fc3', 10, '2016-03-25 08:35:51', NULL, '1', '2016-03-25 08:35:51', '2016-03-25 08:35:51');

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE `artist` (
  `id_artist` int(11) NOT NULL,
  `nama_artist` varchar(50) NOT NULL,
  `id_genre` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`id_artist`, `nama_artist`, `id_genre`, `created_at`, `updated_at`) VALUES
(26, 'Radja', 24, '2016-02-12 22:05:53', NULL),
(27, 'Peterpan', 25, '2016-03-25 13:58:30', NULL),
(28, 'Sheila on 7', 25, '2016-03-25 13:58:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `authority`
--

CREATE TABLE `authority` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `desc` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authority`
--

INSERT INTO `authority` (`id`, `name`, `desc`, `created_at`, `updated_at`) VALUES
(10, 'Superadmin', NULL, '2016-03-03 13:08:05', '2016-03-25 07:44:49');

-- --------------------------------------------------------

--
-- Table structure for table `authority_detail`
--

CREATE TABLE `authority_detail` (
  `authority_id` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `view` char(1) NOT NULL,
  `add` char(1) DEFAULT NULL,
  `edit` char(1) DEFAULT NULL,
  `delete` char(1) DEFAULT NULL,
  `type` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authority_detail`
--

INSERT INTO `authority_detail` (`authority_id`, `id_menu`, `view`, `add`, `edit`, `delete`, `type`) VALUES
(10, 29, 't', NULL, NULL, NULL, '1'),
(10, 30, 't', NULL, NULL, NULL, '1'),
(10, 32, 't', NULL, NULL, NULL, '1'),
(10, 33, 't', NULL, NULL, NULL, '1'),
(10, 14, 't', 't', 't', 't', '2'),
(10, 22, 't', 't', 't', 't', '2'),
(10, 23, 't', 't', 't', 't', '2'),
(10, 29, 't', 't', 't', 't', '2'),
(10, 38, 't', 't', 't', 't', '2'),
(10, 43, 't', 't', 't', 't', '2'),
(10, 44, 't', 't', 't', 't', '2'),
(10, 45, 't', 't', 't', 't', '2');

-- --------------------------------------------------------

--
-- Table structure for table `cd`
--

CREATE TABLE `cd` (
  `kode_cd` int(11) NOT NULL,
  `id_artist` int(11) NOT NULL,
  `release` date NOT NULL,
  `judul_album` varchar(50) NOT NULL,
  `list_lagu` text NOT NULL,
  `keterangan_cd` text NOT NULL,
  `id_label` int(11) NOT NULL,
  `gambar_cd` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cd`
--

INSERT INTO `cd` (`kode_cd`, `id_artist`, `release`, `judul_album`, `list_lagu`, `keterangan_cd`, `id_label`, `gambar_cd`, `harga`, `stok`, `created_at`, `updated_at`) VALUES
(16, 26, '2016-02-01', 'Mengenangmu', '<p>\r\n\r\nAdd the layout-top-nav class to the body tag to get this layout. This feature can also be used with a sidebar! So use this class if you want to remove the custom dropdown menus from the navbar and use regular links instead.\r\n\r\n<br></p>', '<p>\r\n\r\nAdd the layout-top-nav class to the body tag to get this layout. This feature can also be used with a sidebar! So use this class if you want to remove the custom dropdown menus from the navbar and use regular links instead.\r\n\r\n\r\n\r\n<br></p>', 17, 'radja.jpg', 2000000, 20, '2016-02-19 12:34:18', '2016-03-25 07:04:15'),
(17, 27, '2016-02-03', 'Tertinggal Jejak', '<p>Song List</p>', '<p>Desc</p>', 18, 'New-Peterpan-Band.jpg', 200000, 12, '2016-03-25 07:04:04', '2016-03-25 07:31:03'),
(18, 28, '2016-03-01', 'Berlari', '<p>Song List</p>', '<p>Desc</p>', 17, '220px-Pejantan_Tangguh.jpg', 200000, 12, '2016-03-25 07:05:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE `genre` (
  `id_genre` int(11) NOT NULL,
  `nama_genre` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id_genre`, `nama_genre`, `created_at`, `updated_at`) VALUES
(23, 'Rockmantic', NULL, '2016-03-07 12:33:24'),
(24, 'SKA', NULL, '2016-03-07 12:33:28'),
(25, 'Pop', '2016-03-25 06:57:04', NULL),
(26, 'Rock', '2016-03-25 06:57:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `group_menu`
--

CREATE TABLE `group_menu` (
  `id` int(11) NOT NULL,
  `orders` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `type` char(1) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `icon` varchar(100) NOT NULL,
  `desc` text NOT NULL,
  `status` char(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_menu`
--

INSERT INTO `group_menu` (`id`, `orders`, `id_menu`, `id_parent`, `name`, `type`, `url`, `icon`, `desc`, `status`, `created_at`, `updated_at`) VALUES
(14, 1, 30, NULL, 'Menu', '1', 'menu', 'fa-circle-o', '', '1', NULL, '2016-02-24 16:33:02'),
(22, 2, 32, NULL, 'Genre', '1', 'genre', 'fa-circle-o', '', '1', '2016-02-12 15:48:10', '2016-03-25 07:44:00'),
(23, 3, 32, NULL, 'Artist', '1', 'artist', 'fa-circle-o', '', '1', '2016-02-12 15:49:04', '2016-03-25 07:43:54'),
(29, 4, 32, NULL, 'CD', '1', 'cd', 'fa-circle-o', '', '1', '2016-02-12 16:01:10', '2016-02-21 03:48:10'),
(38, 1, 29, NULL, 'Dashboard', '1', 'dashboard', 'fa-circle-o', '', '1', '2016-02-20 10:54:37', '2016-03-02 12:08:43'),
(43, 1, 32, NULL, 'Label', '1', 'label', 'fa-circle-o', '', '1', '2016-02-21 03:47:17', '2016-03-25 07:43:39'),
(44, 2, 30, NULL, 'Authority', '1', 'authority', 'fa-circle-o', '', '1', '2016-03-02 12:18:59', NULL),
(45, 1, 33, NULL, 'City', '1', 'city', 'fa-circle-o', '', '1', '2016-03-25 07:42:02', '2016-03-25 08:03:44');

-- --------------------------------------------------------

--
-- Table structure for table `konfirmasi_pembayaran`
--

CREATE TABLE `konfirmasi_pembayaran` (
  `id_konfirmasi` int(11) NOT NULL,
  `id_rekening` int(11) NOT NULL,
  `id_order` varchar(12) NOT NULL,
  `nama_bank_tp` varchar(50) NOT NULL,
  `no_rekening_tp` varchar(20) NOT NULL,
  `status_bayar` enum('1','2') NOT NULL,
  `jml_transfer` int(11) NOT NULL,
  `tgl_konfirmasi` date NOT NULL,
  `jam_konfirmasi` time NOT NULL,
  `bukti_bayar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id_kota` int(11) NOT NULL,
  `nama_kota` varchar(50) NOT NULL,
  `ongkos_kirim` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id_kota`, `nama_kota`, `ongkos_kirim`, `created_at`, `updated_at`) VALUES
(7, 'Bandung', 100000, '2016-03-25 08:23:29', '2016-03-25 08:27:12'),
(8, 'Jakarta', 200000, '2016-03-25 08:25:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `label`
--

CREATE TABLE `label` (
  `id_label` int(11) NOT NULL,
  `nama_label` varchar(50) NOT NULL,
  `alamat_label` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `label`
--

INSERT INTO `label` (`id_label`, `nama_label`, `alamat_label`, `created_at`, `updated_at`) VALUES
(17, 'Get Label', 'JL.Herkules 3 No.15 Komplek Melong Green Garden. Cimahi Selatan', '2016-02-25 15:19:38', '2016-02-25 15:19:38'),
(18, 'Jack Label', 'Jalan Hercules 3 No 15', '2016-02-24 16:31:37', '2016-02-24 16:31:37');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `nama_member` varchar(50) NOT NULL,
  `password_member` varchar(50) NOT NULL,
  `email_member` varchar(50) NOT NULL,
  `jk` enum('1','2') NOT NULL,
  `no_tlp_member` varchar(20) NOT NULL,
  `alamat_member` text NOT NULL,
  `id_kota` int(11) NOT NULL,
  `kode_pos` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `orders` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `desc` text NOT NULL,
  `status` char(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `orders`, `name`, `icon`, `desc`, `status`, `created_at`, `updated_at`) VALUES
(29, 1, 'Dashboard', 'fa-dashboard', 'Dashboard', '1', '2016-01-30 09:19:13', '2016-02-12 14:23:51'),
(30, 3, 'Settings', 'fa-gears', 'Settings', '1', '2016-01-30 09:20:29', '2016-03-02 16:26:20'),
(32, 2, 'CD', 'fa-music', 'CD', '1', '2016-02-12 15:47:43', '2016-02-12 16:03:07'),
(33, 4, 'Master', 'fa-gears', '', '1', '2016-03-25 07:41:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id_order` varchar(12) NOT NULL,
  `tgl_order` date NOT NULL,
  `jam_order` time NOT NULL,
  `email_member` varchar(50) NOT NULL,
  `status_order` enum('1','2','3','4','5') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `id_order_detail` int(11) NOT NULL,
  `id_order` varchar(12) NOT NULL,
  `kode_cd` int(11) NOT NULL,
  `jumlah` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`id_order_detail`, `id_order`, `kode_cd`, `jumlah`) VALUES
(55, '201503030001', 39, '1'),
(56, '201503030002', 35, '3'),
(57, '201503050001', 42, '1'),
(58, '201503050001', 37, '3'),
(59, '201503050001', 33, '1'),
(60, '201503070001', 31, '4'),
(61, '201503070001', 29, '3'),
(62, '201503070001', 37, '1'),
(63, '201503070002', 36, '1'),
(64, '201503240001', 37, '3');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id_pesan` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subjek` varchar(50) NOT NULL,
  `isi_pesan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id_pesan`, `nama`, `email`, `subjek`, `isi_pesan`) VALUES
(3, 'Ghitha Dinan', 'ghithadinanhs@gmail.com', 'Sembalap', 'Test pesan');

-- --------------------------------------------------------

--
-- Table structure for table `profil_website`
--

CREATE TABLE `profil_website` (
  `id_profil` int(11) NOT NULL,
  `alamat_perusahaan` text,
  `no_tlp_perusahaan` varchar(20) DEFAULT NULL,
  `email_perusahaan` varchar(50) DEFAULT NULL,
  `profil` text,
  `peta_lokasi` text NOT NULL,
  `cara_pemesanan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profil_website`
--

INSERT INTO `profil_website` (`id_profil`, `alamat_perusahaan`, `no_tlp_perusahaan`, `email_perusahaan`, `profil`, `peta_lokasi`, `cara_pemesanan`) VALUES
(1, 'JL.Herkules 3 No.15 Komplek Melong Green Garden, Cimahi Selatan', '0226156174910', 'ghithadinanhs@gmail.com', '<p style="text-align: justify;"><strong>Kisah Klasik Untuk Masa Depan</strong> adalah album <a title="Musik" href="http://id.wikipedia.org/wiki/Musik">musik</a> karya <a title="Sheila on 7" href="http://id.wikipedia.org/wiki/Sheila_on_7">Sheila on 7</a>. Dirilis pada tahun <a title="2000" href="http://id.wikipedia.org/wiki/2000">2000</a>. Dengan <em>hits single</em> lagu <em>Sahabat Sejati</em>, <em>Bila Kau Tak Disampingku</em>, dan <em>Sephia</em>.Album yang ditunggu-tunggu para sheila gank ini berhasil ''meledak'' di pasaran kaset saat itu, bahkan beberapa lagunya sempat menjadi soundtrack beberapa sinetron di televisi swasta nasional.</p>\r\n<p style="text-align: justify;">Kesuksesan Sheila on 7 lewat album pertama membuat pecinta musik Indonesia penasaran dengan wonder band yang satu ini. Buktinya jauh-jauh hari sebelum album keduanya beredar kasetnya sudah dipesan 200 ribu kopi. Begitu single pertama &ldquo;Bila Kau Tak Disampingku&rdquo; dilepas pada 29 september 2000, seperti sudah diduga banyak orang lagu mereka segera menjadi top request di radio. Namun yang menjadi hits terbesar dan paling fenomenal dari album ini adalah single kedua mereka &ldquo;Sephia&rdquo; yang berhasil merajai chart di seluruh Indonesia bahkan sebelum video klipnya diputar di tv. Populernya lagu &ldquo;Sephia&rdquo; pun menarik minat sebuah PH untuk dijadikan sinetron, maka muncullah sinetron Sephia yang kemudian mengangkat nama Marcella Zalianty. Tak hanya sampai disitu, lagu terbaik versi AMI Sharp Award 2001 ini pun menarik minat penyanyi gaek Taiwan Chyi Chin untuk menyanyikan lagu itu dalam bahasa Taiwan. Maka jadilah lagu karya Eross Chandra ini wara wiri di MTV China dengan judul baru &ldquo;Sophia&rdquo;. Dalam waktu kurang dari setahun, tepatnya agustus 2001 album ini telah mencapai angka penjualan 1,7 juta keping di Indonesia saja. Hingga saat ini total penjualan album &ldquo;Kisah klasik Untuk Masa Depan&rdquo; mencapai lebih dari 2 Juta keping, termasuk dari Malaysia, Brunei, dan Singapura. Di Malaysia sendiri album ini terjual lebih dari 150 ribu keping, sebuah angka yang sulit dicapai oleh musisi Malaysia sekalipun. Maka tak heran bila album Kisah Klasik Untuk Masa Depan menjadi No.1 Hits of the World di Chart Billboard Malaysia.</p>', 'https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=id&amp;q=hotel+candra+cimahi&amp;aq=&amp;sll=-6.877508,107.548341&amp;sspn=0.002287,0.002411&amp;t=h&amp;ie=UTF8&amp;filter=0&amp;st=109453761419646176680&amp;rq=1&amp;ev=a&amp;split=1&amp;radius=0.1&amp;hq=hotel+candra+cimahi&amp;ll=-6.877508,107.548341&amp;spn=0.002287,0.002411&amp;z=14&amp;iwloc=A&amp;cid=4428305326489831776&amp;output=embed', '<p style="text-align: justify;">Untuk dapat melakukan pemesanan, anda harus mendaftar terlebih dahulu sebagai member, jika belum silahkan daftar terlebih dahulu <a href="login">di sini</a>. <br /><br /> Jika anda sudah mendaftar sebagai member, untuk melihat data CD yang tersedia dan melakukan pemesanan anda dapat berkunjung pada halaman <a href="produk">produk</a>. Kemudian jika anda telah melakukan pemesanan, anda dapat melihat data CD yang anda pesan pada halamat <a href="cart">cart</a>.<br /><br /> Selanjutnya setelah berhasil melakukan proses pemesanan CD anda di haruskan untuk segera melakukan transfer pembayaran minimal sebesar 50% dari total biaya pemesanan. Jika anda telah melakukan trasfer pembayaran kemudian anda dapat melihat data history pemesanan anda pada menu&nbsp;<a href="history_pesanan">history pesanan</a> untuk dapat melakukan proses konfirmasi pembayaran yang nantinya akan di proses oleh admin. Jika dalam waktu 1X24 anda belum melakukan transaksi pembayaran dan tidak melakukan proses konfirmasi pembayaran maka pesanan anda akan kami anggap batal. <br /><br /> Jika semua ketentutan - ketentuan di atas telah terpenuhi, maka anda selanjutnya dapat <em>mencetak form bukti pemesanan </em>yang terdapat pada halaman <a href="history_pesanan">history pesanan</a> yang nantinya wajib untuk di perlihatkan ke petugas yang akan mengantarkan CD pesanan anda sebagai tanda bukti.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `rekening`
--

CREATE TABLE `rekening` (
  `id_rekening` int(11) NOT NULL,
  `nama_bank` varchar(50) NOT NULL,
  `no_rekening` varchar(20) NOT NULL,
  `gambar_bank` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rekening`
--

INSERT INTO `rekening` (`id_rekening`, `nama_bank`, `no_rekening`, `gambar_bank`) VALUES
(1, 'BCA', '7890000759', 'BCA.jpg'),
(2, 'Mandiri', '1520004643330', 'Mandiri.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
  ADD PRIMARY KEY (`id_artist`),
  ADD KEY `id_genre` (`id_genre`);

--
-- Indexes for table `authority`
--
ALTER TABLE `authority`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `authority_detail`
--
ALTER TABLE `authority_detail`
  ADD KEY `authority_id` (`authority_id`),
  ADD KEY `id_menu` (`id_menu`);

--
-- Indexes for table `cd`
--
ALTER TABLE `cd`
  ADD PRIMARY KEY (`kode_cd`),
  ADD KEY `id_label` (`id_label`),
  ADD KEY `id_artist` (`id_artist`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id_genre`);

--
-- Indexes for table `group_menu`
--
ALTER TABLE `group_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_menu` (`id_menu`);

--
-- Indexes for table `konfirmasi_pembayaran`
--
ALTER TABLE `konfirmasi_pembayaran`
  ADD PRIMARY KEY (`id_konfirmasi`),
  ADD KEY `id_rekening` (`id_rekening`),
  ADD KEY `id_order` (`id_order`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `label`
--
ALTER TABLE `label`
  ADD PRIMARY KEY (`id_label`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`email_member`),
  ADD KEY `id_kota` (`id_kota`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_member` (`email_member`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id_order_detail`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `kode_cd` (`kode_cd`),
  ADD KEY `id_order_2` (`id_order`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id_pesan`);

--
-- Indexes for table `profil_website`
--
ALTER TABLE `profil_website`
  ADD PRIMARY KEY (`id_profil`);

--
-- Indexes for table `rekening`
--
ALTER TABLE `rekening`
  ADD PRIMARY KEY (`id_rekening`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `artist`
--
ALTER TABLE `artist`
  MODIFY `id_artist` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `authority`
--
ALTER TABLE `authority`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cd`
--
ALTER TABLE `cd`
  MODIFY `kode_cd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `genre`
--
ALTER TABLE `genre`
  MODIFY `id_genre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `group_menu`
--
ALTER TABLE `group_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `konfirmasi_pembayaran`
--
ALTER TABLE `konfirmasi_pembayaran`
  MODIFY `id_konfirmasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id_kota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `label`
--
ALTER TABLE `label`
  MODIFY `id_label` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id_order_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id_pesan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `rekening`
--
ALTER TABLE `rekening`
  MODIFY `id_rekening` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `artist`
--
ALTER TABLE `artist`
  ADD CONSTRAINT `artist_ibfk_1` FOREIGN KEY (`id_genre`) REFERENCES `genre` (`id_genre`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `authority_detail`
--
ALTER TABLE `authority_detail`
  ADD CONSTRAINT `authority_detail_ibfk_1` FOREIGN KEY (`authority_id`) REFERENCES `authority` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cd`
--
ALTER TABLE `cd`
  ADD CONSTRAINT `cd_ibfk_1` FOREIGN KEY (`id_label`) REFERENCES `label` (`id_label`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cd_ibfk_2` FOREIGN KEY (`id_artist`) REFERENCES `artist` (`id_artist`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `group_menu`
--
ALTER TABLE `group_menu`
  ADD CONSTRAINT `group_menu_ibfk_1` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `konfirmasi_pembayaran`
--
ALTER TABLE `konfirmasi_pembayaran`
  ADD CONSTRAINT `konfirmasi_pembayaran_ibfk_1` FOREIGN KEY (`id_rekening`) REFERENCES `rekening` (`id_rekening`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `konfirmasi_pembayaran_ibfk_2` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `member`
--
ALTER TABLE `member`
  ADD CONSTRAINT `member_ibfk_1` FOREIGN KEY (`id_kota`) REFERENCES `kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`email_member`) REFERENCES `member` (`email_member`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
